//
//  ViewController.swift
//  ChatbotSample
//
//  Created by Manish Kasodariya on 22/11/21.
//

import UIKit
import ChatbotFramework
class ViewController: UIViewController {
    var btn = ChatButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn.setBackgroundColor = .gray
        btn.UpdateSize = CGSize(width: 70.0, height: 70.0)
        btn.setDistance = 20
        btn.position = .custom
        btn.customPosition = CGPoint(x: 200, y: 500)
        btn.addImage = UIImage(named: "icnBot")
        
        self.view.addSubview(btn)
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnSendMessage(_ sender: UIButton) {
        btn.sendMessage(msgStr: "test", customer_state: "")
        
    }

    func sendFeedback(){
        btn.submitFeedback(rate: 5, feedback: "") { success, error in
            
        }
    }
    
    @IBAction func onBtnLogout(_ sender: UIButton) {
        btn.logout()
        self.navigationController?.popViewController(animated: true)
    }
    
}

