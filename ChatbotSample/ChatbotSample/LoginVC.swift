//
//  LoginVC.swift
//  chatbot-ios
//
//  Created by Manish's Mac on 28/09/21.
//

import UIKit
import ChatbotFramework

class LoginVC: UIViewController {

    @IBOutlet weak var txtUserEmail: UITextField!
    @IBOutlet weak var txtUserPassword: UITextField!
    @IBOutlet weak var lblResponse: UILabel!
    
    var daveChatBotSdk = DaveChatBotSdk(enterpriseId: "enterprice id here", conversationId: "conversation id here", roles: "roles here")
        
    let configSettings = ChatConfigModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtUserEmail.text = ""//enter email address here...
        txtUserPassword.text = ""//enter password here...
        
        if daveChatBotSdk.isAlreadyLogin(){
            let vc = self.storyboard!.instantiateViewController(identifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let settings = ChatSettingModel()
        settings.AssistantName = "Ask Me"
        daveChatBotSdk.setCustomUISettings(setting: settings)
        
//        settings.ChatViewBgColor = .black
//        settings.AssistantProfile = UIImage(named: "icnBot")
//        settings.MessageBgColor = .yellow
//        settings.TypeTextfieldPlaceholderText = "Type here...."
//        settings.QuickChatFont = UIFont.systemFont(ofSize: 20)
//        settings.QuickChatTextColor = .yellow
//        settings.QuickChatListBgColor = .red
//        settings.QuickChatTitle = "Quick chat"
//
//        daveChatBotSdk.setCustomUISettings(setting: settings)
//
//
        let configSettings = ChatConfigModel()
        configSettings.LOGIN_ATTRS = ["email","mobile_number","user_id"]
        configSettings.BASE_API_URL = "" //Set your base url here.
        daveChatBotSdk.setConfigSettings(config: configSettings)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblResponse.text = ""
    }
    
    @IBAction func onBtnLogin(_ sender: UIButton) {
        self.lblResponse.text = ""
        daveChatBotSdk.login(email: txtUserEmail.text!, password: txtUserPassword.text!) { response in
            if let dict = response as? NSDictionary{
                print(dict)
                DispatchQueue.main.async {
                    let vc = self.storyboard!.instantiateViewController(identifier: "ViewController") as! ViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        } failure: { error in
            DispatchQueue.main.async {
            self.lblResponse.text = error
            }
        } exception: { error in
            DispatchQueue.main.async {
            self.lblResponse.text = error
            }
        }
    }
}
