//
//	ClsLoginAuthModel.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 07/09/21.
//

import Foundation


class ClsLoginAuth : NSObject, NSCoding{

	var apiKey : String!
	var enterpriseId : String!
	var pushToken : String!
	var role : String!
	var userId : String!
    var email : String!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		apiKey = dictionary["api_key"] as? String == nil ? "" : dictionary["api_key"] as? String
		enterpriseId = dictionary["enterprise_id"] as? String == nil ? "" : dictionary["enterprise_id"] as? String
		pushToken = dictionary["push_token"] as? String == nil ? "" : dictionary["push_token"] as? String
		role = dictionary["role"] as? String == nil ? "" : dictionary["role"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
        email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if apiKey != nil{
			dictionary["api_key"] = apiKey
		}
		if enterpriseId != nil{
			dictionary["enterprise_id"] = enterpriseId
		}
		if pushToken != nil{
			dictionary["push_token"] = pushToken
		}
		if role != nil{
			dictionary["role"] = role
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
        if email != nil{
            dictionary["email"] = email
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiKey = aDecoder.decodeObject(forKey: "api_key") as? String
         enterpriseId = aDecoder.decodeObject(forKey: "enterprise_id") as? String
         pushToken = aDecoder.decodeObject(forKey: "push_token") as? String
         role = aDecoder.decodeObject(forKey: "role") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if apiKey != nil{
			aCoder.encode(apiKey, forKey: "api_key")
		}
		if enterpriseId != nil{
			aCoder.encode(enterpriseId, forKey: "enterprise_id")
		}
		if pushToken != nil{
			aCoder.encode(pushToken, forKey: "push_token")
		}
		if role != nil{
			aCoder.encode(role, forKey: "role")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
	}

}
