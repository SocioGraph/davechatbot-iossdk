//
//  FeedbackVC.swift
//  ChatbotFramework
//
//  Created by Manish Kasodariya on 24/11/21.
//

import UIKit

class FeedbackVC: UIViewController {
    //MARK:- UI nib
    static var UI : FeedbackVC {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            
            return UIStoryboard(name: "chatStoryboard", bundle: bundle!).instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        }
    }
    @IBOutlet weak var topSafeAreaView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblAssistantName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgBotProfile: UIImageView!{
        didSet{
            imgBotProfile.layer.cornerRadius = imgBotProfile.bounds.width / 2.0
        }
    }
    
    @IBOutlet weak var feedbackHelpful: FloatRatingView!
    
    @IBOutlet weak var feedbackAccurate: FloatRatingView!
    
    @IBOutlet weak var txtFeedback: KMPlaceholderTextView!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblAlertMsg: UILabel!
    
    var didFeedbackSent:(Bool) -> Void = {_ in}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupDesign()
        // Do any additional setup after loading the view.
    }
    
    func setupDesign(){
        topSafeAreaView.backgroundColor = chatScreenSettings.HeaderBarColor
        headerView.backgroundColor = chatScreenSettings.HeaderBarColor
        self.lblAssistantName.text = chatScreenSettings.AssistantName
        
        if let img = chatScreenSettings.CloseChatBgImage.maskWithColor(color: .red) {
            self.btnClose.setImage(img, for: .normal)
        }
        else{
            self.btnClose.setImage(chatScreenSettings.CloseChatBgImage, for: .normal)
        }
        
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        
        if let img = UIImage(named: "StarEmpty", in: bundle, compatibleWith: nil)!.maskWithColor(color: chatScreenSettings.FeedbackStarColor) {
            feedbackHelpful.emptyImage = img
            feedbackAccurate.emptyImage = img
        }
        
        if let img = UIImage(named: "StarFull", in: bundle, compatibleWith: nil)!.maskWithColor(color: chatScreenSettings.FeedbackStarColor) {
            feedbackHelpful.fullImage = img
            feedbackAccurate.fullImage = img
        }
        
        self.view.backgroundColor = chatScreenSettings.ChatViewBgColor
        self.imgBotProfile.image = chatScreenSettings.AssistantProfile
        
    }

    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.didFeedbackSent(false)
        }
    }
    
    @IBAction func onBtnSendFeedback(_ sender: UIButton) {
        if Int(feedbackAccurate.rating) == 0 || Int(feedbackHelpful.rating) == 0{
            lblAlertMsg.isHidden = false
            lblAlertMsg.text = "Please Do Give Star Ratings!"
            lblAlertMsg.textColor = .red
            return
        }
        txtFeedback.resignFirstResponder()
        self.btnClose.isUserInteractionEnabled = false
        self.btnSend.isUserInteractionEnabled = false
        sendFeedback { status in
            DispatchQueue.main.async {
                self.btnClose.isUserInteractionEnabled = true
                self.btnSend.isUserInteractionEnabled = true
                if status{
                    self.lblAlertMsg.isHidden = false
                    self.lblAlertMsg.text = "Thank you for your valuable feedback"
                    self.lblAlertMsg.textColor = .green
                    self.btnClose.isUserInteractionEnabled = false
                    self.btnSend.isUserInteractionEnabled = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                        self.dismiss(animated: false) {
                            self.didFeedbackSent(true)
                        }
                    }
                }
            }
        }
    }
    
    func sendFeedback(completion: @escaping (_ response: Bool) -> Void){
        var request = URLRequest(url: URL(string: BASE_URL + API.FEEDBACK_API + "/" + GlobalVars.engagementId)!)
        request.httpMethod = "PATCH"
        
        for (key,value) in ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                            "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                            "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
                           ] {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let params = ["accuracy_rating":Int(feedbackAccurate.rating),"usefulness_rating":Int(feedbackHelpful.rating),"feedback":txtFeedback.text!] as [String : Any]

        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    completion(true)
                }
                else{
                    completion(false)
                }
            }
            else{
                print(error.debugDescription)
                completion(false)
            }
        })

        task.resume()
    }
}
