//
//  ConversationWordCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 21/10/21.
//

import UIKit

class ConversationWordCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "ConversationWordCell", bundle: bundle)
        }
    }
    
    @IBOutlet weak var lblWord: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    func setupUI(){
//        lblWord.textColor = chatScreenSettings.OptionsTextColor
//        lblWord.font = chatScreenSettings.OptionsFont
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
