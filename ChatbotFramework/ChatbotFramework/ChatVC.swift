//
//  ChatVC.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 02/09/21.
//

import UIKit

class ChatVC: UIViewController {
    //MARK:- UI nib
    static var UI : ChatVC {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            
            return UIStoryboard(name: "chatStoryboard", bundle: bundle!).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        }
    }
    
    
    //MARK:- Outlet
    @IBOutlet weak var lblAssistantName: UILabel!
    
    @IBOutlet weak var topSafeAreaView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var quickChatCollection: UICollectionView!
    @IBOutlet weak var imgBotProfile: UIImageView!{
        didSet{
            imgBotProfile.layer.cornerRadius = imgBotProfile.bounds.width / 2.0
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtMessage: UITextField!{
        didSet{
            txtMessage.delegate = self
        }
    }
    @IBOutlet weak var lblQuickChatTitle: UILabel!
    
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var constraintMessageViewToSuperView: NSLayoutConstraint!
    
    @IBOutlet weak var tblConversationWord: UITableView!
    @IBOutlet weak var heightConversationWordTable: NSLayoutConstraint!
    
    //MARK:- Veriable declaration
    var conversationModel:ClsConversationModel!
    var quickChatArr:[quickChatKeyValue] = []
    var cellsArr:[UITableViewCell] = []
        
    var autoSuggestionWordArr:[ClsKeyValue] = []
    
    var quickChatTitle:String = "this is option"
    
    var heightAutoSuggestionCell = 40.0
    
    var userResponseTimer:Timer!
    var currentTime = 0
    var totalWaitingTime = 0
    var follow_up = ""
    var isRequestSent = false
    var isFirstTimeScroll = true
    //MARK:- UIView life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(keyboardShowNotification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handle(keyboardHideNotification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        setupUI()
        setupDesign()
        // Do any additional setup after loading the view.
    }
    
    func setupDesign(){
        topSafeAreaView.backgroundColor = chatScreenSettings.HeaderBarColor
        headerView.backgroundColor = chatScreenSettings.HeaderBarColor
        
        self.lblQuickChatTitle.text = chatScreenSettings.QuickChatTitle
        
        self.lblAssistantName.text = chatScreenSettings.AssistantName
        
        if let img = chatScreenSettings.CloseChatBgImage.maskWithColor(color: .red) {
            self.btnClose.setImage(img, for: .normal)
        }
        else{
            self.btnClose.setImage(chatScreenSettings.CloseChatBgImage, for: .normal)
        }
        self.view.backgroundColor = chatScreenSettings.ChatViewBgColor
        self.imgBotProfile.image = chatScreenSettings.AssistantProfile
        
        self.txtMessage.placeholder = chatScreenSettings.TypeTextfieldPlaceholderText
        self.txtMessage.textColor = chatScreenSettings.TypeTextfieldTextColor
        self.txtMessage.font = chatScreenSettings.TypeTextfieldTextFont
        
        
    }
    
    func setupUI() {
        
        
        tblChat.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        tblChat.register(LoadingReceiverCell.nib, forCellReuseIdentifier: "LoadingReceiverCell")
        tblChat.register(LoadingSenderCell.nib, forCellReuseIdentifier: "LoadingSenderCell")
        tblChat.register(ChatReceiverCell.nib, forCellReuseIdentifier: "ChatReceiverCell")
        tblChat.register(ChatSenderCell1.nib, forCellReuseIdentifier: "ChatSenderCell1")
        tblChat.register(VideoMsgCell.nib, forCellReuseIdentifier: "VideoMsgCell")
        tblChat.register(CollectionCell.nib, forCellReuseIdentifier: "CollectionCell")
        tblChat.register(ImageCollectionCell.nib, forCellReuseIdentifier: "ImageCollectionCell")
        tblChat.register(FormCell.nib, forCellReuseIdentifier: "FormCell")
        tblChat.register(UrlCell.nib, forCellReuseIdentifier: "UrlCell")
        tblConversationWord.register(ConversationWordCell.nib, forCellReuseIdentifier: "ConversationWordCell")
        tblConversationWord.separatorStyle = .none
        tblChat.separatorStyle = .none
        
        self.lblQuickChatTitle.text = self.quickChatTitle
        
        
        
        messagesArr.removeAll()
        if let previous_arr = getCustomObject(key: "messages_arr") as? [NSDictionary], previous_arr.count > 0 {
            if let previous_arr = getCustomObject(key: "messages_arr") as? [NSDictionary]{
                for dict in previous_arr {
                    let model = ClsConversationModel(fromDictionary: dict)
                    if model.direction == "user" && model.customerResponse == ""{}else{
                        messagesArr.append(ClsConversationModel(fromDictionary: dict))
                    }
                }
            }
            
//            getQuickCheckList()
            
            loadChatMessageInTable()
        }
        else{
            loadChatMessageInTable()
        }
        
    }

    func getHistoryData(){
        getConversationHistory { messagesAr in
            
            let messages = messagesAr.reversed()
            
            for msg in messages {
                messagesArr.insert(msg, at: 0)
            }
            
            self.storeMessages()
            
            DispatchQueue.main.async {
//                self.quickChatCollection.reloadData()
                self.tblChat.reloadData()
//                self.loadChatMessageInTable()
            }
        }
    }
    
    func loadChatMessageInTable(){
        reloadQuickChat()
        if GlobalVars.engagementId == ""{
            sendInitialMessage()
        }
        else{
            
//            self.systemResponse = messagesArr.last?.systemResponse ?? ""
//            self.engagementId = messagesArr.last?.engagementId ?? ""
            self.tblChat.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                self.tblChat.scrollToBottom()
                self.isFirstTimeScroll = false
            }
        }
    }
    
    func sendInitialMessage(){
        let msg = ClsConversationModel(fromDictionary: ["direction" : "system"])
        msg.isPlaceholder = true
        messagesArr.append(msg)
        self.tblChat.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.tblChat.scrollToBottom()
            self.isFirstTimeScroll = false
        }
        
        let header:NSDictionary = ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                                   "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                                   "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
        ]
        
        getConversationText(header: header)
    }

    func reloadQuickChat(){
        if GlobalVars.quickChatMsg != nil{
            self.quickChatArr.removeAll()
            for (key,value) in GlobalVars.quickChatMsg {
                self.quickChatArr.append(quickChatKeyValue(key: key as! String, value: value as! String))
            }
            self.quickChatCollection.setContentOffset(.zero, animated: false)
            self.quickChatCollection.reloadData()
        }
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.stopTimer()
        let vc = FeedbackVC.UI
        vc.didFeedbackSent = { isSent in
            DispatchQueue.main.async {            
                self.stopTimer()
                self.dismiss(animated: true)
            }
        }
        self.present(vc, animated: false)
        
//        self.dismiss(animated: true)
    }
    
    @objc private func handle(keyboardHideNotification notification: Notification) {
        self.constraintMessageViewToSuperView.constant = 10
        UIView.animate(withDuration: 0.05) {
            self.view.layoutIfNeeded()
        }
    }
    @objc private func handle(keyboardShowNotification notification: Notification) {
        // 1
        print("Keyboard show notification")
        
        // 2
        if let userInfo = notification.userInfo,
            // 3
            let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            print(keyboardRectangle.height)
            self.constraintMessageViewToSuperView.constant = keyboardRectangle.height;
            UIView.animate(withDuration: 0.05) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func onBtnSendMessage(_ sender: UIButton) {
        startUserInteractionTimer()
        txtMessage.resignFirstResponder()
        if txtMessage.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return
        }
        
        messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
        
        
        if let quichMsg = self.quickChatArr.first(where: {$0.value.lowercased() == txtMessage.text!.lowercased()}){
            self.sendMessage(msgStr: quichMsg.value,customer_state: quichMsg.key)
        }
        else if txtMessage.text!.lowercased() == "start testing chatbot"{
            GlobalVars.systemResponse = "sr_test_cases"
            self.sendMessage(msgStr: "start testing chatbot")
        }
        else{
            self.sendMessage(msgStr: txtMessage.text!)
        }
        
        self.isShowAutoSuggestionTableView(isShow: false)
        
        self.txtMessage.text = ""
    }
    
    func addUserPlaceholder(){
        
        messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
        
        let r_msg = ClsConversationModel(fromDictionary: NSDictionary())
        r_msg.isPlaceholder = true
        r_msg.isSend = true
        r_msg.direction = "user"
        r_msg.timestamp = Date().getCurrentGMTTime()
        messagesArr.append(r_msg)
        
        self.tblChat.reloadData()
        self.tblChat.scrollToBottom()
    }
    
    func removeUserPlaceholder(isReload:Bool = true){
        messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
        
        if isReload{
            self.tblChat.reloadData()
            self.tblChat.scrollToBottom()
        }
    }
    
    public func sendTempMessage(){
        
    }
    
    func sendMessage(msgStr:String,customer_state:String = "",requestModel:ClsConversationModel? = nil,displayMsg:String? = nil){
        let msg = ClsConversationModel(fromDictionary: NSDictionary())
        msg.customerResponse = msgStr
        if let model = requestModel, model.data.responseType == "form"{
            
            var str = ""
            if let dict = msgStr.convertToDictionary(){
                for formModel in model.data.form{
                    if str != "" {str.append("\n")}
                    if let value = dict[formModel.name] as? String, let key = formModel.title{
                        str.append("\(key) : \(value)")
                    }
                    if formModel.name == "geoloc"{
                        for (key,value) in dict {
                            if str != "" {str.append("\n")}
                            str.append("\(key) : \(value)")
                        }
                    }
                }
            }
            msg.customerResponse = str
        }
        if let display_msg = displayMsg{
            msg.customerResponse = display_msg
        }
        msg.isSend = true
        msg.direction = "user"
        msg.timestamp = Date().getCurrentGMTTime()
        if msg.customerResponse != ""{
            messagesArr.append(msg)
        }
        
        storeMessages()
        
        let r_msg = ClsConversationModel(fromDictionary: NSDictionary())
        r_msg.placeholder = ""
        r_msg.isPlaceholder = true
        r_msg.direction = "system"
        r_msg.timestamp = Date().getCurrentGMTTime()
        messagesArr.append(r_msg)
        
        DispatchQueue.main.async {
            self.tblChat.reloadData()
            self.tblChat.scrollToBottom()
        }
        let header:NSDictionary = ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                                   "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                                   "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
        ]
        
        
        
        getConversationText(text: msgStr,customerState: customer_state, header: header)
    }
    
    func storeMessages(){
        var dict:[NSDictionary] = []
        for model in messagesArr {
            dict.append(model.toDictionary())
        }
        setCustomObject(value: dict as AnyObject, key: "messages_arr")
    }
    
    func findAutoSuggesationWord(type:String){
        if type.count >= 3{
            var autoWords:[ClsKeyValue] = []
            for model in GlobalVars.conversationKeywordArr {
                let arr = model.value.components(separatedBy: " ").map({$0.lowercased()})
                if arr.contains(type.lowercased()){
                    autoWords.append(model)
                }
            }
            self.autoSuggestionWordArr = autoWords
            self.isShowAutoSuggestionTableView()
        }
        else{
            autoSuggestionWordArr = []
            self.isShowAutoSuggestionTableView(isShow: false)
        }
    }
    
    func isShowAutoSuggestionTableView(isShow:Bool = true){
        if isShow{
            tblConversationWord.reloadData()
            heightConversationWordTable.constant = Double(self.autoSuggestionWordArr.count) * heightAutoSuggestionCell
        }
        else{
            heightConversationWordTable.constant = 0
            tblConversationWord.reloadData()
        }
        self.view.layoutIfNeeded()
    }
    
    
    func startUserInteractionTimer(){
        stopTimer()
        if self.totalWaitingTime == 0{
            return
        }
        self.currentTime = 0
        userResponseTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(sendMessageForUserInteraction), userInfo: nil, repeats: true)
    }
    
    func stopTimer(){
        if userResponseTimer != nil{
            self.userResponseTimer.invalidate()
            self.userResponseTimer = nil
        }
    }
    
    @objc func sendMessageForUserInteraction(){
        if totalWaitingTime > 0{
            self.currentTime += 1
            print(self.currentTime)
            if self.currentTime >= totalWaitingTime{
                self.currentTime = 0
                if follow_up != ""{
                    self.sendMessage(msgStr: "", customer_state: follow_up)
                }
            }
        }
    }
    
}




extension ChatVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblConversationWord {
            return autoSuggestionWordArr.count
        }
        return messagesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblConversationWord {
            let cell = tblConversationWord.dequeueReusableCell(withIdentifier: "ConversationWordCell") as! ConversationWordCell
            cell.lblWord.text = autoSuggestionWordArr[indexPath.row].key
            return cell
        }
        let model = messagesArr[indexPath.row]
        if model.isPlaceholder{
            if model.isSend{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "LoadingSenderCell") as! LoadingSenderCell
                let frameworkBundleID  = "chatbotFramework";
                let bundle = Bundle(identifier: frameworkBundleID)
                let imageData = try? Data(contentsOf: bundle!.url(forResource: "typing", withExtension: "gif")!)

                if let img = UIImage.gifImageWithData(imageData ?? Data()){
                    cell.imgLoader.image = img
                }
                return cell
            }
            let cell = tblChat.dequeueReusableCell(withIdentifier: "LoadingReceiverCell") as! LoadingReceiverCell
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            let imageData = try? Data(contentsOf: bundle!.url(forResource: "typing", withExtension: "gif")!)

            if let img = UIImage.gifImageWithData(imageData ?? Data()){
                cell.imgLoader.image = img
            }
            return cell
        }
        else if model.direction == "user"{
            let cell = tblChat.dequeueReusableCell(withIdentifier: "ChatSenderCell1") as! ChatSenderCell1
            cell.lblMessage.text = model.customerResponse
            if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
            }
            else{
                cell.lblTime.text = model.timestamp.getDateFromFormat()
            }
            return cell
        }
        else{
            let placeholder = model.placeholder.replacingOccurrences(of: "<br />", with: "\n")
            
            if model.data.responseType == "url"{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "UrlCell") as! UrlCell
                cell.lblMessage.text = placeholder
                if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                    cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
                }
                else{
                    cell.lblTime.text = model.timestamp.getDateFromFormat()
                }
                var imageArr:[String] = []
                for imgModel in model.data.thumbnails {
                    imageArr.append(imgModel.image)
                }
                cell.setupCell(model.data.url)
                return cell
            }
            if model.data.responseType == "form"{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "FormCell") as! FormCell
                cell.lblMessage.text = placeholder
                cell.customerState = model.data.customerState
                if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                    cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
                }
                else{
                    cell.lblTime.text = model.timestamp.getDateFromFormat()
                }
                var imageArr:[String] = []
                for imgModel in model.data.thumbnails {
                    imageArr.append(imgModel.image)
                }
                cell.tag = indexPath.row
                cell.setupCell(model.data.form)
                
                if !model.data.isEnable{
                    cell.stackView.isUserInteractionEnabled = false
                    cell.stackView.alpha = 0.5
                }
                return cell
            }
            if model.data.responseType == "thumbnails"{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "ImageCollectionCell") as! ImageCollectionCell
                cell.lblMessage.text = placeholder
                if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                    cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
                }
                else{
                    cell.lblTime.text = model.timestamp.getDateFromFormat()
                }
                cell.setupCell(imageArr: model.data.thumbnails)
                return cell
            }
            if model.data.responseType == "image"{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "ImageCollectionCell") as! ImageCollectionCell
                cell.lblMessage.text = placeholder
                if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                    cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
                }
                else{
                    cell.lblTime.text = model.timestamp.getDateFromFormat()
                }
                cell.setupCell(imageArr: [ClsThumbnail(fromDictionary: ["image":model.data.image as AnyObject])])
                return cell
            }
            if model.data.responseType == "options"{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "CollectionCell") as! CollectionCell
                cell.tag = indexPath.row
                cell.lblMessage.text = placeholder
                if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                    cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
                }
                else{
                    cell.lblTime.text = model.timestamp.getDateFromFormat()
                }
                if model.data.optionsList.count > 0 {
                    cell.stackView.axis = .vertical
                    cell.stackView.distribution = .fill
                    cell.setupCell(model.data.optionsList)
                }
                else if model.data.options.count > 0{
                    var optionsArr:[[String]] = []
                    for option in model.data.options {
                        optionsArr.append(["",option])
                    }
                    cell.stackView.axis = .horizontal
                    cell.stackView.distribution = .fillEqually
                    cell.setupCell(optionsArr)
                }
                cell.didSelect = { btn, index in
                    if let label = btn.accessibilityLabel{
                        if let value = btn.accessibilityValue, value != ""{
                            self.sendMessage(msgStr: label, customer_state: value)
                        }
                        else{
                            if let customerState = messagesArr[index].customerState{
                                self.sendMessage(msgStr: label, customer_state: customerState)
                            }
                        }
                    }
                }
                return cell
            }
            if model.data.responseType == "video"{
                let cell = tblChat.dequeueReusableCell(withIdentifier: "VideoMsgCell") as! VideoMsgCell
                cell.setupView(model.data.video)
                cell.lblMessage.text = placeholder
                if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                    cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
                }
                else{
                    cell.lblTime.text = model.timestamp.getDateFromFormat()
                }
                return cell
            }
            let cell = tblChat.dequeueReusableCell(withIdentifier: "ChatReceiverCell") as! ChatReceiverCell
            cell.lblMessage.text = placeholder
            if model.timestamp.getDateFromFormat(toFormat: "dd/MM/yyyy") == Date().getCurrentGMTTime(format: "dd/MM/yyyy"){
                cell.lblTime.text = model.timestamp.getDateFromFormat(toFormat:"HH:mm")
            }
            else{
                cell.lblTime.text = model.timestamp.getDateFromFormat()
            }
            return cell
        }
        let cell = cellsArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblConversationWord {
            return heightAutoSuggestionCell
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblConversationWord{
            
            txtMessage.resignFirstResponder()
            if txtMessage.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
                return
            }
            
            messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
            
            self.sendMessage(msgStr: autoSuggestionWordArr[indexPath.row].key,customer_state: autoSuggestionWordArr[indexPath.row].type)
            
            txtMessage.text = ""
            
            self.isShowAutoSuggestionTableView(isShow: false)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && !isFirstTimeScroll{
            getHistoryData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        startUserInteractionTimer()
    }
}

extension ChatVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        startUserInteractionTimer()
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        self.findAutoSuggesationWord(type: newString)
        if newString != ""{
            if messagesArr.filter({$0.isSend == true && $0.isPlaceholder == true}).count > 0 {
                return true
            }
            let r_msg = ClsConversationModel(fromDictionary: NSDictionary())
            r_msg.isPlaceholder = true
            r_msg.isSend = true
            r_msg.direction = "user"
            r_msg.timestamp = Date().getCurrentGMTTime()
            messagesArr.append(r_msg)
            
            self.tblChat.reloadData()
            self.tblChat.scrollToBottom()
        }
        else if newString == ""{
            if messagesArr.filter({$0.isSend == true && $0.isPlaceholder == true}).count > 0 {
                messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
                self.tblChat.reloadData()
                self.tblChat.scrollToBottom()
            }
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
        self.tblChat.reloadData()
        self.tblChat.scrollToBottom()
    }
}




//MARK:- Collectionview Delegate, Datasource
extension ChatVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quickChatArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuickChatCell", for: indexPath) as! QuickChatCell
        cell.lblQuickText.text = self.quickChatArr[indexPath.row].value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        txtMessage.resignFirstResponder()
        
        messagesArr.removeAll(where: {$0.isSend == true && $0.isPlaceholder == true})
        
        self.sendMessage(msgStr: self.quickChatArr[indexPath.row].value,customer_state: self.quickChatArr[indexPath.row].key)
        
    }

}

//MARK:- API
extension ChatVC{
    func getConversationText(text:String = "a",customerState:String = "",header:NSDictionary = [:]){
        

        var request = URLRequest(url: URL(string: BASE_URL + API.CONVERSATION_API + "/" + "aslkdfjalskjdf")!)
        request.httpMethod = "POST"
        for (key,value) in header {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        if GlobalVars.engagementId != "" {
            let params = ["system_response":GlobalVars.systemResponse,"engagement_id":GlobalVars.engagementId,"customer_state":customerState,"customer_response":text]
            
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                            self.conversationModel = ClsConversationModel(fromDictionary: json as NSDictionary)
                            if self.conversationModel.wait > 0{
                                self.totalWaitingTime = self.conversationModel.wait / 1000
                                self.follow_up = self.conversationModel.data.followUps.first ?? ""
                                self.startUserInteractionTimer()
                            }else{
                                self.totalWaitingTime = 0
                                self.stopTimer()
                            }
                            self.conversationModel.direction = "system"
                            GlobalVars.systemResponse = self.conversationModel.name
                            GlobalVars.engagementId = self.conversationModel.engagementId
                            
                            messagesArr.removeAll(where: {$0.isPlaceholder == true})
                            
//                            let msg = chatMessage(fromDictionary: NSDictionary())
//                            msg.message = self.conversationModel.placeholder
//                            msg.senderName = "Aldo Virtual Assistant"
//                            msg.systemResponse = self.conversationModel.name
//                            msg.engagementId = self.conversationModel.engagementId
//                            msg.date = Date()
//                            msg.data = self.conversationModel.data
                            messagesArr.append(self.conversationModel)
                            
                            
                            let dict = self.conversationModel.toDictionary()
                            
                            self.storeMessages()
                            DispatchQueue.main.async {
                                
                                if self.txtMessage.isEditing && self.txtMessage.text != ""{
                                    let r_msg = ClsConversationModel(fromDictionary: NSDictionary())
//                                    r_msg.message = ""
//                                    r_msg.senderName = ""
                                    r_msg.isPlaceholder = true
                                    r_msg.isSend = true
                                    r_msg.direction = "user"
                                    r_msg.timestamp = Date().getCurrentGMTTime()
                                    messagesArr.append(r_msg)
                                }
                                
                                self.tblChat.reloadData()
                                self.tblChat.scrollToBottom()
                            }
                            
                            GlobalVars.quickChatMsg = self.conversationModel.stateOptions
                            DispatchQueue.main.async {
                                self.reloadQuickChat()
                                self.startUserInteractionTimer()
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                else{
                    messagesArr.removeAll(where: {$0.isPlaceholder == true})
                    
                    let r_msg = ClsConversationModel(fromDictionary: NSDictionary())
                    r_msg.placeholder = "There seems to be some technical issue in the system. Please try a different query."
                    r_msg.direction = "system"
                    r_msg.timestamp = Date().getCurrentGMTTime()
                    messagesArr.append(r_msg)
                    
                    
                    self.storeMessages()
                    
                    DispatchQueue.main.async {
                        self.tblChat.reloadData()
                        self.tblChat.scrollToBottom()
                    }
                }
            }
            else{
                print(error.debugDescription)
            }
        })

        task.resume()
    }
    
    
    func getConversationHistory(completion: @escaping (_ response: [ClsConversationModel]) -> Void){
        
        if isRequestSent == true || (getCustomObject(key: "isFinishedRecord") as? Bool) == true{
            return
        }
        isRequestSent = true
        let pageNumber = (getCustomObject(key: "_page_number") as? Int ?? 1) + 1
        
        var request = URLRequest(url: URL(string: BASE_URL + API.CONVERSATION_HISTORY_API + "/" + GlobalVars.objLoginModel.userId + "?_page_size=20&_page_number=\(pageNumber)")!)
        request.httpMethod = "GET"
        
        for (key,value) in ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                            "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                            "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
                           ] {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            var modelArr:[ClsConversationModel] = []
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                            self.isRequestSent = false
                            if let historyArr = json["history"] as? [NSDictionary], historyArr.count > 0{
                                setCustomObject(value: pageNumber as AnyObject, key: "_page_number")
                                for dict in historyArr {
                                    let msg = ClsConversationModel(fromDictionary: dict)
                                    modelArr.append(msg)
                                }
                            }
                            else{
                                setCustomObject(value: true as AnyObject, key: "isFinishedRecord")
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                else{
                    
                }
            }
            else{
                print(error.debugDescription)
            }
            completion(modelArr)
        })

        task.resume()
    }
}


//MARK:- QuickChatCell
class QuickChatCell: UICollectionViewCell {
    @IBOutlet weak var lblQuickText: UILabel!
    
    override func awakeFromNib() {
        setupUI()
    }
    
    func setupUI(){
        lblQuickText.font = chatScreenSettings.QuickChatFont
        lblQuickText.textColor = chatScreenSettings.QuickChatTextColor
        self.contentView.backgroundColor = chatScreenSettings.QuickChatListBgColor
    }
}

class quickChatKeyValue {
    var key:String = ""
    var value:String = ""
    init(key:String,value:String) {
        self.key = key
        self.value = value
    }
}


