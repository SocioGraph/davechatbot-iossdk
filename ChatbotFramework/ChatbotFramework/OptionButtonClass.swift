//
//  OptionButtonClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit

public class OptionButtonClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblOption: UILabel!
    
    
    var didSelect:(OptionButtonClass) -> Void = {_ in}
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionButtonClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        
        setupUI()
    }
    
    func setupUI(){
        lblOption.textColor = chatScreenSettings.MessageFontColor
        lblOption.font = chatScreenSettings.MessageFont
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    @IBAction func onBtnTap(_ sender: UIButton) {
        didSelect(self)
    }
    
}
