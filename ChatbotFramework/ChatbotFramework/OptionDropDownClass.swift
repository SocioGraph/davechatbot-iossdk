//
//  OptionDropDownClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit
import Foundation

public class OptionDropDownClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var txtOptions: UITextField!
    
    var optionsArr:[ClsKeyValue] = []
    
    var formModel:ClsFormModel!{
        didSet{
            self.optionsArr.removeAll()
            for strArr in formModel.options {
                if strArr.count > 1{
                    self.optionsArr.append(ClsKeyValue(key: strArr[0], value: strArr[1]))
                }
            }
        }
    }
    
    fileprivate let pickerView = ToolbarPickerView()
    
    var selectedRow = 0
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionDropDownClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        
        
        self.txtOptions.inputView = self.pickerView
        self.txtOptions.inputAccessoryView = self.pickerView.toolbar
        self.txtOptions.delegate = self
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.pickerView.toolbarDelegate = self
        
        self.pickerView.reloadAllComponents()
    }
    
    func setupUI(){
        txtOptions.textColor = chatScreenSettings.MessageFontColor
        txtOptions.font = chatScreenSettings.MessageFont
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
}
extension OptionDropDownClass:UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.isEditing ? false : true
    }
}

extension OptionDropDownClass: UIPickerViewDataSource, UIPickerViewDelegate {

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.optionsArr.count
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.optionsArr[row].value
    }

    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
//        self.txtOptions.text = self.titles[row]
    }
}

extension OptionDropDownClass: ToolbarPickerViewDelegate {

    func didTapDone() {
        txtOptions.resignFirstResponder()
        txtOptions.text = self.optionsArr[selectedRow].value
        self.formModel.text = self.optionsArr[selectedRow].value
    }

    func didTapCancel() {
        txtOptions.resignFirstResponder()
    }
}


protocol ToolbarPickerViewDelegate:class {
    func didTapDone()
    func didTapCancel()
}

class ToolbarPickerView: UIPickerView {

    public private(set) var toolbar: UIToolbar?
    public weak var toolbarDelegate: ToolbarPickerViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    private func commonInit() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelTapped))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        self.toolbar = toolBar
    }

    @objc func doneTapped() {
        self.toolbarDelegate?.didTapDone()
    }

    @objc func cancelTapped() {
        self.toolbarDelegate?.didTapCancel()
    }
}


public class ClsKeyValue{
    var key:String!
    var value:String!
    var type:String!
    init(key:String,value:String,type:String = ""){
        self.key = key
        self.value = value
        self.type = type
    }
}
