//
//  OptionButtonClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit
import MapKit
public class OptionLocationClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
        
    @IBOutlet weak var txtLocation: UITextField!{
        didSet{
            txtLocation.delegate = self
        }
    }
    
    @IBOutlet weak var mapView: MKMapView!
    
    var formModel:ClsFormModel!{
        didSet{
            txtLocation.placeholder = formModel.placeholder
            if formModel.value != ""{
                if formModel.value.contains(","){
                    if formModel.value.components(separatedBy: ",").count > 1{
                        let lat = Double(formModel.value.components(separatedBy: ",")[0]) ?? 0.0
                        let log = Double(formModel.value.components(separatedBy: ",")[1]) ?? 0.0
                        
                        addAnnotationPin(locName: formModel.text, locCoordinate: CLLocationCoordinate2D(latitude: lat, longitude: log))
                    }
                }
            }
        }
    }
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionLocationClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        setupUI()
    }
    
    func setupUI(){
        
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    func addAnnotationPin(locName:String,locCoordinate:CLLocationCoordinate2D){
        self.txtLocation.text = locName
        
        self.formModel.text = locName
        
        self.formModel.value = "\(locCoordinate.latitude),\(locCoordinate.longitude)"
        if self.mapView.annotations.count > 0{
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        
        let pin = MKPointAnnotation()
        pin.title = locName
        pin.coordinate = locCoordinate
        self.mapView.addAnnotation(pin)
        
        let viewRegion = MKCoordinateRegion(center: locCoordinate, latitudinalMeters: 200, longitudinalMeters: 200)
        self.mapView.setRegion(viewRegion, animated: false)
    }
    
}

extension OptionLocationClass:UITextFieldDelegate{
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if let vc = self.parentVC as? ChatVC{
            let vcc = SearchLocationVC.UI
            vcc.didSelect = { item in
                DispatchQueue.main.async {
                    if let location = item.placemark.location{
                        self.addAnnotationPin(locName: item.placemark.title!, locCoordinate: location.coordinate)
                    }
                }
            }
            vc.present(vcc, animated: true)
        }
        textField.resignFirstResponder()
    }
}
