//
//  OptionButtonClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit

public class OptionTextViewClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var textView: KMPlaceholderTextView!{
        didSet{
            textView.delegate = self
        }
    }
    
    var formModel:ClsFormModel!
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionTextViewClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        setupUI()
    }
    
    func setupUI(){
        textView.textColor = chatScreenSettings.MessageFontColor
        textView.font = chatScreenSettings.MessageFont
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    
}

extension OptionTextViewClass:UITextViewDelegate{
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let vc = self.parentVC as? ChatVC{
            vc.startUserInteractionTimer()
        }
        let newString = (textView.text! as NSString).replacingCharacters(in: range, with: text)
        formModel.text = newString
        return true
    }
}
