//
//  LoaderVC.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 05/10/21.
//

import UIKit

class LoaderVC: UIViewController {
    //MARK:- UI nib
    static var UI : LoaderVC {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            
            return UIStoryboard(name: "chatStoryboard", bundle: bundle!).instantiateViewController(withIdentifier: "LoaderVC") as! LoaderVC
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
