//
//  OptionButtonClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit

public class SubmitButtonClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("SubmitButtonClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        setupUI()
    }
    
    func setupUI(){
        btnSubmit.setTitleColor(chatScreenSettings.MessageFontColor, for: .normal)
        btnSubmit.titleLabel!.font = chatScreenSettings.MessageFont
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    
}
