//
//  Constant.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 07/09/21.
//

import Foundation
import UIKit
import AVFoundation

let defaults = UserDefaults.standard
let defaultNotification = NotificationCenter.default
var messagesArr:[ClsConversationModel] = []
var chatScreenSettings:ChatSettingModel! = ChatSettingModel()
var chatConfigSettings:ChatConfigModel! = ChatConfigModel()
var BASE_URL = ""
var CONVERSATION_ID = ""
//MARK:- Variables
class GlobalVars {
    static var USER_LOGIN_INFO = "USER_LOGIN_INFO"
    static var ENGAGEMENT_ID = "ENGAGEMENT_ID"
    static var RESPONSE_ID = "RESPONSE_ID"
    
    static var api_key = ""
    static var user_id = ""
    static var enterprise_id = ""
    static var email = ""
    
    static var objLoginModel = ClsLoginAuthModel()
    
    static var systemResponse:String = ""
    static var engagementId:String = ""
    
    static var quickChatMsg:NSDictionary!
    
    static var conversationKeywordArr:[ClsKeyValue] = []
}


//MARK:- API URLs
class API {
    static var AUTH_API = "dave/oauth"
    static var CONVERSATION_API = "conversation/\(CONVERSATION_ID)"
    static var CONVERSATION_HISTORY_API = "conversation-history/\(CONVERSATION_ID)"
    static var CONVERSATION_KEYWORD_API = "conversation-keywords/\(CONVERSATION_ID)"
    static var FEEDBACK_API = "conversation-feedback/dave"
    static var UPLOAD_DOCUMENT_API = "upload_file?large_file=true"
}








// MARK: - Custom object Functions Save User Default
func removeObjectForKey(_ objectKey: String) {
   
   let defaults = UserDefaults.standard
   defaults.removeObject(forKey: objectKey)
   defaults.synchronize()
}

func setCustomObject(value:AnyObject,key:String)
{
   let data = NSKeyedArchiver.archivedData(withRootObject: value)
   UserDefaults.standard.set(data, forKey: key)
   UserDefaults.standard.synchronize()
}

func getCustomObject(key:String) -> Any?
{
   let data = UserDefaults.standard.object(forKey: key) as? NSData
   if data == nil
   {
       return nil
   }
   let value = NSKeyedUnarchiver.unarchiveObject(with: data! as Data)
   return value
}

extension Date{
    func getCurrentGMTTime(format:String = "yyyy-MM-dd HH:mm:ss a +0000") -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        print(dateFormatter.string(from: Date()))
        return dateFormatter.string(from: Date())
    }
}


extension String{
    func getDateFromFormat(format:String = "yyyy-MM-dd HH:mm:ss a +0000", toFormat:String = "MMM dd, HH:mm") -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let date = dateFormatter.date(from: self)
        
        let toFormatter = DateFormatter()
        toFormatter.dateFormat = toFormat
        return toFormatter.string(from: date ?? Date())
    }
    
    func getDateFromLocalFormat(format:String = "yyyy-MM-dd", toFormat:String = "MMM dd, HH:mm") -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.current
        let date = dateFormatter.date(from: self)
        
        let toFormatter = DateFormatter()
        toFormatter.dateFormat = toFormat
        return toFormatter.string(from: date ?? Date())
    }
    
    func getDateFromFormat(format:String = "yyyy-MM-dd HH:mm:ss a +0000") -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension Date{
    func getStrDateFromDate(format:String = "yyyy-MM-dd hh:mm") ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: self)
    }
}

extension UIImageView {
    func changeColor(_ color:UIColor) {
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
extension UIImage {

    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!

        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!

        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)

        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }

}
extension UIView {
    func setRadius(radius: CGFloat, corners:CACornerMask = [.layerMinXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner],borderColor:UIColor = .clear,borderWidth:CGFloat = 0.0) {
//        layerMaxXMaxYCorner – lower right corner
//        layerMaxXMinYCorner – top right corner
//        layerMinXMaxYCorner – lower left corner
//        layerMinXMinYCorner – top left corner
        if #available(iOS 11.0, *) {
            layer.maskedCorners = corners
        } else {
            // Fallback on earlier versions
        }
        layer.cornerRadius  = radius
        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }
    
    var parentVC: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}


extension UITableView {
    var tableViewHeight: CGFloat {
        layoutIfNeeded()
        return contentSize.height
    }
    
    func scrollToBottom(isAnimate:Bool = false){
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections-1) - 1,
                section: self.numberOfSections - 1)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .bottom, animated: isAnimate)
            }
            self.isPagingEnabled = false
        }
    }
    
    func scrollToTop(isAnimate:Bool = false) {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .top, animated: isAnimate)
            }
        }
    }
    
    func hasRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
    }
}

extension String{
    var empty: Bool {
        get {
            return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
        }
    }
    
    var string:String{
        get{
            return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    var validEmail : Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    var isPhoneNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
        var filtered:NSString!
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        filtered = inputString.componentsJoined(by: "") as NSString
        return  self == filtered as String
        
    }
}

extension UIViewController {
    func alertOkay(title:String, message:String, okAction: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okay  = UIAlertAction(title: "Okay", style: UIAlertAction.Style.cancel) { (action) in
            okAction?()
        }
        
        alert.addAction(okay)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertConfirm(title:String, message:String, buttonTitles:[String], yesAction:(() -> Void)? = nil, noAction:(() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let yesAction  = UIAlertAction(title: buttonTitles[0], style: UIAlertAction.Style.default) { (action) in
            yesAction?()
        }
        
        let noAction  = UIAlertAction(title: buttonTitles[1], style: UIAlertAction.Style.cancel) { (action) in
            noAction?()
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension Dictionary {
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }

        return String(data: theJSONData, encoding: .ascii)
    }
}

extension UITextField {
    
    func addInputViewDatePicker(target: Any, selector: Selector,isOnlyDate:Bool = false,selectedDate:Date? = nil,minDate:Date? = nil,maxDate:Date? = nil) {
        
        let screenWidth = UIScreen.main.bounds.width
        
        //Add DatePicker as inputView
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = (isOnlyDate ? .date : .dateAndTime)
        if let minDate = minDate{
            datePicker.minimumDate = minDate
        }
        if let maxDate = maxDate{
            datePicker.maximumDate = maxDate
        }
        if let selectedDate = selectedDate{
            datePicker.setDate(selectedDate, animated: false)
        }
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        self.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
    @objc func cancelPressed() {
        self.resignFirstResponder()
    }
}

extension URL {
    var attributes: [FileAttributeKey : Any]? {
        do {
            return try FileManager.default.attributesOfItem(atPath: path)
        } catch let error as NSError {
            print("FileAttribute error: \(error)")
        }
        return nil
    }

    var fileSize: UInt64 {
        return attributes?[.size] as? UInt64 ?? UInt64(0)
    }
    
    var fileSizeString: String {
        return ByteCountFormatter.string(fromByteCount: Int64(fileSize), countStyle: .file)
    }

    var creationDate: Date? {
        return attributes?[.creationDate] as? Date
    }
    
    var fileName:String{
        self.lastPathComponent
    }
    
    var sizePerMB:Double {
        return NSNumber(value: fileSize).doubleValue / 1000000.0
    }
    
    func loadFileFromLocalPath() ->Data? {
       return try? Data(contentsOf: self)
    }
}


extension FileManager {

    func getDocumentsInFolder() -> [URL]{
        let documentDirURL = NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask, true).first!
        let directoryURL = documentDirURL.appending("/MyDocuments")
        let dirContents = try? FileManager.default.contentsOfDirectory(atPath: directoryURL)
        var fileUrlsArr:[URL] = []
        if let filesList = dirContents{
            for file in filesList {
                let documentDirURL2 = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                let fileURL = documentDirURL2.appendingPathComponent("MyDocuments").appendingPathComponent(file)
                if FileManager.default.fileExists(atPath: fileURL.path) {
                    fileUrlsArr.append(fileURL)
                }
            }
        }
        return fileUrlsArr
    }
    
    func deleteFile(fileURL:URL) -> Bool{
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do{
                try FileManager.default.removeItem(at: fileURL)
            } catch (let error){
                print("Cannot delete item : \(error)")
                return false
            }
            return true
        }
        return false
    }
    
    func secureCopyItem(at srcURL: URL, to dstURL: URL) -> Bool {
        
        do {
            if FileManager.default.fileExists(atPath: dstURL.path) {
                try FileManager.default.removeItem(at: dstURL)
            }
            try FileManager.default.copyItem(at: srcURL, to: dstURL)
        } catch (let error) {
            print("Cannot copy item at \(srcURL) to \(dstURL): \(error)")
            return false
        }
        return true
    }
    
    func moveFile(fromUrl:URL) -> URL? {
        let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentDirURL.appendingPathComponent("MyDocuments").appendingPathComponent(fromUrl.fileName)
        if !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.createDirectory(atPath: fileURL.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        if secureCopyItem(at: fromUrl, to: fileURL){
            print("File PAth: \(fileURL.path)")
            return fileURL
        }
        return nil
    }

}
