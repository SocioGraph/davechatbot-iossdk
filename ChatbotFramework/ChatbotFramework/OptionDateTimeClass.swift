//
//  OptionButtonClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit

public class OptionDateTimeClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var textField: UITextField!
    
    var formModel:ClsFormModel!
    var selectedDate:Date!
    var isOnlyDate:Bool = false
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionDateTimeClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        setupUI()
    }
    
    func setupUI(){
        textField.textColor = chatScreenSettings.MessageFontColor
        textField.font = chatScreenSettings.MessageFont
    }
    
    func setupView(_ value:String, minDateStr:String, maxDateStr:String) {
        var minDate:Date? = nil
        if minDateStr != ""{
            minDate = minDateStr.getDateFromFormat(format:(isOnlyDate ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm"))
        }
        
        var maxDate:Date? = nil
        if maxDateStr != ""{
            maxDate = maxDateStr.getDateFromFormat(format:(isOnlyDate ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm"))
        }
        if value != "" {
            selectedDate = value.getDateFromFormat(format:((isOnlyDate ? "yyyy-MM-dd" : (value.contains(":") ? "yyyy-MM-dd HH:mm" : "yyyy-MM-dd"))))
        }
        self.textField.addInputViewDatePicker(target: self, selector: #selector(done(_:)),isOnlyDate: isOnlyDate, selectedDate: selectedDate, minDate: minDate, maxDate: maxDate)
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    @IBAction func done(_ sender: UIButton) {
        if let vc = self.parentVC as? ChatVC{
            vc.startUserInteractionTimer()
        }
        if let  datePicker = self.textField.inputView as? UIDatePicker {
            self.textField.text = datePicker.date.getStrDateFromDate(format: (isOnlyDate ? "dd/MM/yyyy" : "dd/MM/yyyy HH:mm"))
            formModel.value = datePicker.date.getStrDateFromDate(format: (isOnlyDate ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm"))
            selectedDate = datePicker.date
        }
        self.textField.resignFirstResponder()
    }
}
