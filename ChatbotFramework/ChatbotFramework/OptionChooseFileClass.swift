//
//  OptionChooseFileClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit
import Foundation
import CoreServices
public class OptionChooseFileClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnChooseFile: UIButton!
    @IBOutlet weak var lblFileName: UILabel!
    
    var formModel:ClsFormModel!{
        didSet{
            self.lblTitle.text = formModel.title.capitalized
        }
    }
        
    var didTapChooseFile:(ClsFormModel,Int) -> Void = {_,_ in}
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionChooseFileClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        btnChooseFile.addTarget(self, action: #selector(chooseFile(_:)), for: .touchUpInside)
        
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    @IBAction func chooseFile(_ sender: UIButton) {
        if let vc = self.parentVC as? ChatVC{
            vc.startUserInteractionTimer()
        }
        let fileTypeArr = formModel.fileType.components(separatedBy: ",")
        var typeArr:[String] = []
        for type in fileTypeArr {
            if type == ".mp3" || type == ".wav"{
                typeArr.append("public.audio")
            }
            if type == ".mp4"{
                typeArr.append("public.video")
            }
            if type == ".avi"{
                typeArr.append("public.movie")
            }
            if type == ".pdf"{
                typeArr.append("com.adobe.pdf")
            }
            if type.contains("image"){
                typeArr.append("public.jpg")
                typeArr.append("public.jpeg")
                typeArr.append("public.png")
            }
        }
        
        openDocument(type: typeArr)
    }
    
    func openDocument(type:[String]) {
        if let vc = view.parentVC as? ChatVC{
            let documentPicker = UIDocumentPickerViewController(documentTypes: type, in: .import)
            documentPicker.delegate = self
            documentPicker.allowsMultipleSelection = false
            if #available(iOS 13.0, *) {
                documentPicker.shouldShowFileExtensions = true
            } else {
                // Fallback on earlier versions
            }
            vc.present(documentPicker, animated: true, completion: nil)
        }
    }
    
    
}


extension OptionChooseFileClass:UIDocumentPickerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let pickFileURL = urls[0]
        if pickFileURL.sizePerMB > Double(formModel.fileMaxSize){
            if let vc = view.parentVC as? ChatVC{
                vc.alertOkay(title: "Alert", message: "File size exceed the maximum allowed size of undefined MB!")
            }
            return
        }
        
        if let url = FileManager.default.moveFile(fromUrl: urls[0]){
            self.formModel.fileUrl = url.absoluteString
            self.lblFileName.text = url.fileName
        }
    }
}
