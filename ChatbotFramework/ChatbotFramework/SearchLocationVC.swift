//
//  SearchLocationVC.swift
//  ChatbotFramework
//
//  Created by Manish Kasodariya on 29/11/21.
//

import UIKit
import MapKit

class SearchLocationVC: UIViewController {
    //MARK:- UI nib
    static var UI : SearchLocationVC {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            
            return UIStoryboard(name: "chatStoryboard", bundle: bundle!).instantiateViewController(withIdentifier: "SearchLocationVC") as! SearchLocationVC
        }
    }
    
    @IBOutlet weak var topSafeAreaView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblAssistantName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgBotProfile: UIImageView!{
        didSet{
            imgBotProfile.layer.cornerRadius = imgBotProfile.bounds.width / 2.0
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    var matchingItems:[MKMapItem] = []
    var didSelect:(MKMapItem) -> Void = {_ in}
    override func viewDidLoad() {
        super.viewDidLoad()
        topSafeAreaView.backgroundColor = chatScreenSettings.HeaderBarColor
        headerView.backgroundColor = chatScreenSettings.HeaderBarColor
        
        self.lblAssistantName.text = chatScreenSettings.AssistantName
        
        if let img = chatScreenSettings.CloseChatBgImage.maskWithColor(color: .red) {
            self.btnClose.setImage(img, for: .normal)
        }
        else{
            self.btnClose.setImage(chatScreenSettings.CloseChatBgImage, for: .normal)
        }
        
        self.view.backgroundColor = chatScreenSettings.ChatViewBgColor
        self.imgBotProfile.image = chatScreenSettings.AssistantProfile
        
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Do any additional setup after loading the view.
    }
    
    func makeRequest(_ text:String){
        if text == ""{
            self.matchingItems.removeAll()
            self.tableView.reloadData()
            return
            
        }
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = text
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            guard let response = response else {
                return
            }
            self.matchingItems = response.mapItems
            self.tableView.reloadData()
        }
    }

    @IBAction func onClose(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

extension SearchLocationVC:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        makeRequest(searchText)
    }
}

extension SearchLocationVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let selectedItem = matchingItems[indexPath.row].placemark
        cell.textLabel?.text = selectedItem.title
        cell.detailTextLabel?.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelect(self.matchingItems[indexPath.row])
        self.dismiss(animated: true)
    }
}
