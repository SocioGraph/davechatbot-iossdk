//
//  OptionButtonClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit

public class OptionTextfieldClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var textField: UITextField!{
        didSet{
            textField.delegate = self
        }
    }
    
    var formModel:ClsFormModel!{
        didSet{
            textField.keyboardType = .default
            if formModel.name == "mobile_number" {
                textField.keyboardType = .phonePad
            }
            if formModel.name == "email"{
                textField.keyboardType = .emailAddress
            }
        }
    }
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionTextfieldClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        setupUI()
        
    }
    
    func setupUI(){
        textField.textColor = chatScreenSettings.MessageFontColor
        textField.font = chatScreenSettings.MessageFont
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    
}


extension OptionTextfieldClass:UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let vc = self.parentVC as? ChatVC{
            vc.startUserInteractionTimer()
        }
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if formModel.uiElement == "number"{
            if newString.count > 0 && newString[newString.index(newString.startIndex, offsetBy: 0)] == "+"{
                if newString.count > 13{
                    return false
                }
            }
            else if newString.count > 10{
                return false
            }
            
            if newString.isPhoneNumber{
                formModel.text = newString
                return true
            }
            return false
        }
        formModel.text = newString
        return true
    }
}
