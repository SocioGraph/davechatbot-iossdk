//
//  OptionImageClass.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit

public class OptionImageClass: UIView {
    //MARK:- Outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblOption: UILabel!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lblImgName1: UILabel!
    @IBOutlet weak var imgView2: UIView!
    @IBOutlet weak var img2: UIImageView!
    
    @IBOutlet weak var lblImgName2: UILabel!
    
    var didSelectImage:(String) -> Void = {_ in}
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func setupUI(){
        lblImgName1.textColor = chatScreenSettings.MessageFontColor
        lblImgName1.font = chatScreenSettings.MessageFont
        
        lblImgName2.textColor = chatScreenSettings.MessageFontColor
        lblImgName2.font = chatScreenSettings.MessageFont
    }
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("OptionImageClass", owner: self, options: nil)
        self.addSubview(view)
        self.addConstraints()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleImage1(_:)))
        self.img1.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleImage2(_:)))
        self.img2.addGestureRecognizer(tap2)
    }
    
    
    @IBAction func handleImage1(_ sender: UIButton) {
        didSelectImage(img1.accessibilityValue ?? "")
    }
    
    @IBAction func handleImage2(_ sender: UIButton) {
        didSelectImage(img2.accessibilityValue ?? "")
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    
}
