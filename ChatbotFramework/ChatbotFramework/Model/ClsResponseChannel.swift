//
//	ClsResponseChannel.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 09/09/21.
//

import Foundation


class ClsResponseChannel : NSObject, NSCoding{

	var frames : String!
	var voice : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		frames = dictionary["frames"] as? String == nil ? "" : dictionary["frames"] as? String
		voice = dictionary["voice"] as? String == nil ? "" : dictionary["voice"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if frames != nil{
			dictionary["frames"] = frames
		}
		if voice != nil{
			dictionary["voice"] = voice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         frames = aDecoder.decodeObject(forKey: "frames") as? String
         voice = aDecoder.decodeObject(forKey: "voice") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if frames != nil{
			aCoder.encode(frames, forKey: "frames")
		}
		if voice != nil{
			aCoder.encode(voice, forKey: "voice")
		}

	}

}
