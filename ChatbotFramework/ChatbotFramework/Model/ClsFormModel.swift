//
//	Form.swift
//
//	Create by MacBook A1398 on 6/10/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ClsFormModel : NSObject, NSCoding{
    
    var error : String!
    var maxDate : String!
    var maxTime : String!
    var minDate : String!
    var minTime : String!
    var required : Bool!
    var timeResolution : String!
    var name : String!
    var placeholder : String!
    var title : String!
    var uiElement : String!
    var text:String!
    var value : String!
    var fileUrl : String!
    var fileType : String!
    var fileMaxSize : Int!
    var options : [[String]]!
    
    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }
    
    /**
     * Overiding init method
     */
    override init(){
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        maxDate = dictionary["max_date"] as? String == nil ? "" : dictionary["max_date"] as? String
        maxTime = dictionary["max_time"] as? String == nil ? "" : dictionary["max_time"] as? String
        minDate = dictionary["min_date"] as? String == nil ? "" : dictionary["min_date"] as? String
        minTime = dictionary["min_time"] as? String == nil ? "" : dictionary["min_time"] as? String
        error = dictionary["error"] as? String == nil ? "" : dictionary["error"] as? String
        name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
        placeholder = dictionary["placeholder"] as? String == nil ? "" : dictionary["placeholder"] as? String
        title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
        uiElement = dictionary["ui_element"] as? String == nil ? "" : dictionary["ui_element"] as? String
        text = dictionary["text"] as? String == nil ? "" : dictionary["text"] as? String
        required = dictionary["required"] as? Bool == nil ? false : dictionary["required"] as? Bool
        timeResolution = dictionary["time_resolution"] as? String == nil ? "" : dictionary["time_resolution"] as? String
        value = dictionary["value"] as? String == nil ? "" : dictionary["value"] as? String
        fileType = dictionary["file_type"] as? String == nil ? "" : dictionary["file_type"] as? String
        fileUrl = dictionary["fileUrl"] as? String == nil ? "" : dictionary["fileUrl"] as? String
        if let fileSize = dictionary["max_file_size"] as? String {
            fileMaxSize = Int(fileSize) ?? 0
        }
        else if let fileSize = dictionary["max_file_size"] as? Int{
            fileMaxSize = fileSize
        }
        options = dictionary["options"] as? [[String]] == nil ? [] : dictionary["options"] as? [[String]]
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if maxDate != nil{
            dictionary["max_date"] = maxDate
        }
        if maxTime != nil{
            dictionary["max_time"] = maxTime
        }
        if minDate != nil{
            dictionary["min_date"] = minDate
        }
        if minTime != nil{
            dictionary["min_time"] = minTime
        }
        if required != nil{
            dictionary["required"] = required
        }
        if timeResolution != nil{
            dictionary["time_resolution"] = timeResolution
        }
        if error != nil{
            dictionary["error"] = error
        }
        if name != nil{
            dictionary["name"] = name
        }
        if placeholder != nil{
            dictionary["placeholder"] = placeholder
        }
        if title != nil{
            dictionary["title"] = title
        }
        if uiElement != nil{
            dictionary["ui_element"] = uiElement
        }
        if text != nil{
            dictionary["text"] = text
        }
        if value != nil{
            dictionary["value"] = value
        }
        if fileType != nil{
            dictionary["file_type"] = fileType
        }
        if fileUrl != nil{
            dictionary["fileUrl"] = fileUrl
        }
        if fileMaxSize != nil{
            dictionary["max_file_size"] = fileMaxSize
        }
        if options != nil{
            dictionary["options"] = options
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        maxDate = aDecoder.decodeObject(forKey: "max_date") as? String
        maxTime = aDecoder.decodeObject(forKey: "max_time") as? String
        minDate = aDecoder.decodeObject(forKey: "min_date") as? String
        minTime = aDecoder.decodeObject(forKey: "min_time") as? String
        error = aDecoder.decodeObject(forKey: "error") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        placeholder = aDecoder.decodeObject(forKey: "placeholder") as? String
        required = aDecoder.decodeObject(forKey: "required") as? Bool
        timeResolution = aDecoder.decodeObject(forKey: "time_resolution") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        uiElement = aDecoder.decodeObject(forKey: "ui_element") as? String
        value = aDecoder.decodeObject(forKey: "value") as? String
        text = aDecoder.decodeObject(forKey: "text") as? String
        fileType = aDecoder.decodeObject(forKey: "file_type") as? String
        fileUrl = aDecoder.decodeObject(forKey: "fileUrl") as? String
        fileMaxSize = aDecoder.decodeObject(forKey: "max_file_size") as? Int
        options = aDecoder.decodeObject(forKey: "options") as? [[String]]
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    public func encode(with aCoder: NSCoder)
    {
        if maxDate != nil{
            aCoder.encode(maxDate, forKey: "max_date")
        }
        if maxTime != nil{
            aCoder.encode(maxTime, forKey: "max_time")
        }
        if minDate != nil{
            aCoder.encode(minDate, forKey: "min_date")
        }
        if minTime != nil{
            aCoder.encode(minTime, forKey: "min_time")
        }
        if error != nil{
            aCoder.encode(error, forKey: "error")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if required != nil{
            aCoder.encode(required, forKey: "required")
        }
        if timeResolution != nil{
            aCoder.encode(timeResolution, forKey: "time_resolution")
        }
        if placeholder != nil{
            aCoder.encode(placeholder, forKey: "placeholder")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if uiElement != nil{
            aCoder.encode(uiElement, forKey: "ui_element")
        }
        if text != nil{
            aCoder.encode(text, forKey: "text")
        }
        if value != nil{
            aCoder.encode(value, forKey: "value")
        }
        if fileType != nil{
            aCoder.encode(fileType, forKey: "file_type")
        }
        if fileUrl != nil{
            aCoder.encode(fileUrl, forKey: "fileUrl")
        }
        if fileMaxSize != nil{
            aCoder.encode(fileMaxSize, forKey: "max_file_size")
        }
        if options != nil{
            aCoder.encode(options, forKey: "options")
        }
    }
    
}
