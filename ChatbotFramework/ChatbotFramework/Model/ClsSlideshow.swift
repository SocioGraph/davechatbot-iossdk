//
//	Slideshow.swift
//
//	Create by MacBook A1398 on 28/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ClsSlideshow : NSObject, NSCoding{

	var caption : String!
	var image : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		caption = dictionary["caption"] as? String == nil ? "" : dictionary["caption"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if caption != nil{
			dictionary["caption"] = caption
		}
		if image != nil{
			dictionary["image"] = image
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         caption = aDecoder.decodeObject(forKey: "caption") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if caption != nil{
			aCoder.encode(caption, forKey: "caption")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}

	}

}
