//
//	ClsData.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 09/09/21.
//

import Foundation


class ClsData : NSObject, NSCoding{

	var followUps : [String]!
	var forceOpen : Bool!
	var openState : String!
	var responseType : String!
    var image : String!
	var target : String!
	var video : String!
    var slideshow : [ClsSlideshow]!
    var thumbnails : [ClsThumbnail]!
    var options : [String]!
    var optionsList : [[String]]!
    var form : [ClsFormModel]!
    var url:String!
    var isEnable:Bool!
    var customerState:String!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
        customerState = dictionary["customer_state"] as? String == nil ? "" : dictionary["customer_state"] as? String
		followUps = dictionary["_follow_ups"] as? [String] == nil ? [] : dictionary["_follow_ups"] as? [String]
		forceOpen = dictionary["_force_open"] as? Bool == nil ? false : dictionary["_force_open"] as? Bool
		openState = dictionary["_open_state"] as? String == nil ? "" : dictionary["_open_state"] as? String
		responseType = dictionary["response_type"] as? String == nil ? "" : dictionary["response_type"] as? String
        image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		target = dictionary["target"] as? String == nil ? "" : dictionary["target"] as? String
		video = dictionary["video"] as? String == nil ? "" : dictionary["video"] as? String
        options = dictionary["options"] as? [String] == nil ? [] : dictionary["options"] as? [String]
        optionsList = dictionary["options"] as? [[String]] == nil ? [] : dictionary["options"] as? [[String]]
        slideshow = [ClsSlideshow]()
        if let slideshowArray = dictionary["slideshow"] as? [NSDictionary]{
            for dic in slideshowArray{
                let value = ClsSlideshow(fromDictionary: dic)
                slideshow.append(value)
            }
        }
        thumbnails = [ClsThumbnail]()
        if let thumbnailsArray = dictionary["thumbnails"] as? [NSDictionary]{
            for dic in thumbnailsArray{
                let value = ClsThumbnail(fromDictionary: dic)
                thumbnails.append(value)
            }
        }
        form = [ClsFormModel]()
        if let formArray = dictionary["form"] as? [NSDictionary]{
            for dic in formArray{
                let value = ClsFormModel(fromDictionary: dic)
                form.append(value)
            }
        }
        url = dictionary["url"] as? String == nil ? "" : dictionary["url"] as? String
        isEnable = dictionary["enable"] as? Bool == nil ? true : dictionary["enable"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        if customerState != nil{
            dictionary["customer_state"] = customerState
        }
		if followUps != nil{
			dictionary["_follow_ups"] = followUps
		}
		if forceOpen != nil{
			dictionary["_force_open"] = forceOpen
		}
		if openState != nil{
			dictionary["_open_state"] = openState
		}
		if responseType != nil{
			dictionary["response_type"] = responseType
		}
        if image != nil{
            dictionary["image"] = image
        }
		if target != nil{
			dictionary["target"] = target
		}
		if video != nil{
			dictionary["video"] = video
		}
        if options != nil{
            dictionary["options"] = options
        }
        if optionsList != nil && options.count == 0{
            dictionary["options"] = optionsList
        }
        if slideshow != nil{
            var dictionaryElements = [NSDictionary]()
            for slideshowElement in slideshow {
                dictionaryElements.append(slideshowElement.toDictionary())
            }
            dictionary["slideshow"] = dictionaryElements
        }
        if thumbnails != nil{
            var dictionaryElements = [NSDictionary]()
            for thumbnailsElement in thumbnails {
                dictionaryElements.append(thumbnailsElement.toDictionary())
            }
            dictionary["thumbnails"] = dictionaryElements
        }
        if form != nil{
            var dictionaryElements = [NSDictionary]()
            for formElement in form {
                dictionaryElements.append(formElement.toDictionary())
            }
            dictionary["form"] = dictionaryElements
        }
        if url != nil{
            dictionary["url"] = url
        }
        if isEnable != nil{
            dictionary["enable"] = isEnable
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        customerState = aDecoder.decodeObject(forKey: "customer_state") as? String
         followUps = aDecoder.decodeObject(forKey: "_follow_ups") as? [String]
         forceOpen = aDecoder.decodeObject(forKey: "_force_open") as? Bool
         openState = aDecoder.decodeObject(forKey: "_open_state") as? String
         responseType = aDecoder.decodeObject(forKey: "response_type") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
         target = aDecoder.decodeObject(forKey: "target") as? String
         video = aDecoder.decodeObject(forKey: "video") as? String
        slideshow = aDecoder.decodeObject(forKey: "slideshow") as? [ClsSlideshow]
        thumbnails = aDecoder.decodeObject(forKey: "thumbnails") as? [ClsThumbnail]
        options = aDecoder.decodeObject(forKey: "options") as? [String]
        optionsList = aDecoder.decodeObject(forKey: "options") as? [[String]]
        form = aDecoder.decodeObject(forKey: "form") as? [ClsFormModel]
        url = aDecoder.decodeObject(forKey: "url") as? String
        isEnable = aDecoder.decodeObject(forKey: "enable") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
        if customerState != nil{
            aCoder.encode(customerState, forKey: "customer_state")
        }
		if followUps != nil{
			aCoder.encode(followUps, forKey: "_follow_ups")
		}
		if forceOpen != nil{
			aCoder.encode(forceOpen, forKey: "_force_open")
		}
		if openState != nil{
			aCoder.encode(openState, forKey: "_open_state")
		}
		if responseType != nil{
			aCoder.encode(responseType, forKey: "response_type")
		}
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
		if target != nil{
			aCoder.encode(target, forKey: "target")
		}
		if video != nil{
			aCoder.encode(video, forKey: "video")
		}
        if slideshow != nil{
            aCoder.encode(slideshow, forKey: "slideshow")
        }
        if thumbnails != nil{
            aCoder.encode(thumbnails, forKey: "thumbnails")
        }
        if options != nil{
            aCoder.encode(options, forKey: "options")
        }
        if optionsList != nil{
            aCoder.encode(optionsList, forKey: "options")
        }
        if form != nil{
            aCoder.encode(form, forKey: "form")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if isEnable != nil{
            aCoder.encode(isEnable, forKey: "enable")
        }
	}

}
