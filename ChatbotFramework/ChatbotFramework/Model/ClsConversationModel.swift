//
//	ClsConversationModel.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 09/09/21.
//

import Foundation


class ClsConversationModel : NSObject, NSCoding{

	var customerState : String!
    var customerResponse : String!
	var data : ClsData!
    var direction : String!
	var engagementId : String!
	var maintainWhiteboard : Bool!
	var name : String!
	var options : String!
	var overwriteWhiteboard : Bool!
	var placeholder : String!
	var placeholderAliases : ClsPlaceholderAliase!
	var responseChannels : ClsResponseChannel!
	var responseId : String!
	var showFeedback : Bool!
	var showInHistory : Bool!
	var stateOptions : NSDictionary!
	var title : String!
	var toStateFunction : ClsToStateFunction!
	var wait : Int!
	var whiteboard : String!
	var whiteboardTitle : String!
    var timestamp : String!
    var isSend:Bool = false
    var isPlaceholder:Bool = false

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		customerState = dictionary["customer_state"] as? String == nil ? "" : dictionary["customer_state"] as? String
        customerResponse = dictionary["customer_response"] as? String == nil ? "" : dictionary["customer_response"] as? String
        direction = dictionary["direction"] as? String == nil ? "" : dictionary["direction"] as? String
		if let dataData = dictionary["data"] as? NSDictionary{
			data = ClsData(fromDictionary: dataData)
		}
		else
		{
			data = ClsData(fromDictionary: NSDictionary.init())
		}
		engagementId = dictionary["engagement_id"] as? String == nil ? "" : dictionary["engagement_id"] as? String
		maintainWhiteboard = dictionary["maintain_whiteboard"] as? Bool == nil ? false : dictionary["maintain_whiteboard"] as? Bool
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		options = dictionary["options"] as? String == nil ? "" : dictionary["options"] as? String
		overwriteWhiteboard = dictionary["overwrite_whiteboard"] as? Bool == nil ? false : dictionary["overwrite_whiteboard"] as? Bool
		placeholder = dictionary["placeholder"] as? String == nil ? "" : dictionary["placeholder"] as? String
		if let placeholderAliasesData = dictionary["placeholder_aliases"] as? NSDictionary{
			placeholderAliases = ClsPlaceholderAliase(fromDictionary: placeholderAliasesData)
		}
		else
		{
			placeholderAliases = ClsPlaceholderAliase(fromDictionary: NSDictionary.init())
		}
		if let responseChannelsData = dictionary["response_channels"] as? NSDictionary{
			responseChannels = ClsResponseChannel(fromDictionary: responseChannelsData)
		}
		else
		{
			responseChannels = ClsResponseChannel(fromDictionary: NSDictionary.init())
		}
		responseId = dictionary["response_id"] as? String == nil ? "" : dictionary["response_id"] as? String
		showFeedback = dictionary["show_feedback"] as? Bool == nil ? false : dictionary["show_feedback"] as? Bool
		showInHistory = dictionary["show_in_history"] as? Bool == nil ? false : dictionary["show_in_history"] as? Bool
		if let stateOptionsData = dictionary["state_options"] as? NSDictionary{
			stateOptions = stateOptionsData
		}
		else
		{
			stateOptions = NSDictionary.init()
		}
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
		if let toStateFunctionData = dictionary["to_state_function"] as? NSDictionary{
			toStateFunction = ClsToStateFunction(fromDictionary: toStateFunctionData)
		}
		else
		{
			toStateFunction = ClsToStateFunction(fromDictionary: NSDictionary.init())
		}
		wait = dictionary["wait"] as? Int == nil ? 0 : dictionary["wait"] as? Int
        timestamp = dictionary["timestamp"] as? String == nil ? "" : dictionary["timestamp"] as? String
		whiteboard = dictionary["whiteboard"] as? String == nil ? "" : dictionary["whiteboard"] as? String
		whiteboardTitle = dictionary["whiteboard_title"] as? String == nil ? "" : dictionary["whiteboard_title"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        if customerResponse != nil{
            dictionary["customer_response"] = customerResponse
        }
		if customerState != nil{
			dictionary["customer_state"] = customerState
		}
        if direction != nil{
            dictionary["direction"] = direction
        }
		if data != nil{
			dictionary["data"] = data.toDictionary()
		}
		if engagementId != nil{
			dictionary["engagement_id"] = engagementId
		}
		if maintainWhiteboard != nil{
			dictionary["maintain_whiteboard"] = maintainWhiteboard
		}
		if name != nil{
			dictionary["name"] = name
		}
		if options != nil{
			dictionary["options"] = options
		}
		if overwriteWhiteboard != nil{
			dictionary["overwrite_whiteboard"] = overwriteWhiteboard
		}
		if placeholder != nil{
			dictionary["placeholder"] = placeholder
		}
		if placeholderAliases != nil{
			dictionary["placeholder_aliases"] = placeholderAliases.toDictionary()
		}
		if responseChannels != nil{
			dictionary["response_channels"] = responseChannels.toDictionary()
		}
		if responseId != nil{
			dictionary["response_id"] = responseId
		}
		if showFeedback != nil{
			dictionary["show_feedback"] = showFeedback
		}
		if showInHistory != nil{
			dictionary["show_in_history"] = showInHistory
		}
		if stateOptions != nil{
			dictionary["state_options"] = stateOptions
		}
		if title != nil{
			dictionary["title"] = title
		}
		if toStateFunction != nil{
			dictionary["to_state_function"] = toStateFunction.toDictionary()
		}
		if wait != nil{
			dictionary["wait"] = wait
		}
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
		if whiteboard != nil{
			dictionary["whiteboard"] = whiteboard
		}
		if whiteboardTitle != nil{
			dictionary["whiteboard_title"] = whiteboardTitle
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        customerResponse = aDecoder.decodeObject(forKey: "customer_response") as? String
         customerState = aDecoder.decodeObject(forKey: "customer_state") as? String
        direction = aDecoder.decodeObject(forKey: "direction") as? String
         data = aDecoder.decodeObject(forKey: "data") as? ClsData
         engagementId = aDecoder.decodeObject(forKey: "engagement_id") as? String
         maintainWhiteboard = aDecoder.decodeObject(forKey: "maintain_whiteboard") as? Bool
         name = aDecoder.decodeObject(forKey: "name") as? String
         options = aDecoder.decodeObject(forKey: "options") as? String
         overwriteWhiteboard = aDecoder.decodeObject(forKey: "overwrite_whiteboard") as? Bool
         placeholder = aDecoder.decodeObject(forKey: "placeholder") as? String
         placeholderAliases = aDecoder.decodeObject(forKey: "placeholder_aliases") as? ClsPlaceholderAliase
         responseChannels = aDecoder.decodeObject(forKey: "response_channels") as? ClsResponseChannel
         responseId = aDecoder.decodeObject(forKey: "response_id") as? String
         showFeedback = aDecoder.decodeObject(forKey: "show_feedback") as? Bool
         showInHistory = aDecoder.decodeObject(forKey: "show_in_history") as? Bool
         stateOptions = aDecoder.decodeObject(forKey: "state_options") as? NSDictionary
         title = aDecoder.decodeObject(forKey: "title") as? String
         toStateFunction = aDecoder.decodeObject(forKey: "to_state_function") as? ClsToStateFunction
        timestamp = aDecoder.decodeObject(forKey: "timestamp") as? String
         wait = aDecoder.decodeObject(forKey: "wait") as? Int
         whiteboard = aDecoder.decodeObject(forKey: "whiteboard") as? String
         whiteboardTitle = aDecoder.decodeObject(forKey: "whiteboard_title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
        if customerResponse != nil{
            aCoder.encode(customerResponse, forKey: "customer_response")
        }
		if customerState != nil{
			aCoder.encode(customerState, forKey: "customer_state")
		}
        if direction != nil{
            aCoder.encode(direction, forKey: "direction")
        }
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if engagementId != nil{
			aCoder.encode(engagementId, forKey: "engagement_id")
		}
		if maintainWhiteboard != nil{
			aCoder.encode(maintainWhiteboard, forKey: "maintain_whiteboard")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if options != nil{
			aCoder.encode(options, forKey: "options")
		}
		if overwriteWhiteboard != nil{
			aCoder.encode(overwriteWhiteboard, forKey: "overwrite_whiteboard")
		}
		if placeholder != nil{
			aCoder.encode(placeholder, forKey: "placeholder")
		}
		if placeholderAliases != nil{
			aCoder.encode(placeholderAliases, forKey: "placeholder_aliases")
		}
		if responseChannels != nil{
			aCoder.encode(responseChannels, forKey: "response_channels")
		}
		if responseId != nil{
			aCoder.encode(responseId, forKey: "response_id")
		}
		if showFeedback != nil{
			aCoder.encode(showFeedback, forKey: "show_feedback")
		}
		if showInHistory != nil{
			aCoder.encode(showInHistory, forKey: "show_in_history")
		}
		if stateOptions != nil{
			aCoder.encode(stateOptions, forKey: "state_options")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if toStateFunction != nil{
			aCoder.encode(toStateFunction, forKey: "to_state_function")
		}
		if wait != nil{
			aCoder.encode(wait, forKey: "wait")
		}
        if timestamp != nil{
            aCoder.encode(timestamp, forKey: "timestamp")
        }
		if whiteboard != nil{
			aCoder.encode(whiteboard, forKey: "whiteboard")
		}
		if whiteboardTitle != nil{
			aCoder.encode(whiteboardTitle, forKey: "whiteboard_title")
		}

	}

}
