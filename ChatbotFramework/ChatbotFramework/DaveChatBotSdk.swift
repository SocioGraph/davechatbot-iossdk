//
//  DaveChatBotSdk.swift
//  ChatbotFramework
//
//  Created by Manish Kasodariya on 23/11/21.
//

import UIKit

public class DaveChatBotSdk{
    public var userEmail:String = ""
    public var userPassword:String = ""
    public var userRole:String = ""
    public var enterpriseId:String = ""
    public var conversationId:String = ""
    
    var settings : ChatSettingModel!{
        didSet{
            chatScreenSettings = settings
        }
    }
    var configSettings : ChatConfigModel!{
        didSet{
            chatConfigSettings = configSettings
            BASE_URL = chatConfigSettings.BASE_API_URL + "/"
        }
    }
    
    public func setCustomUISettings(setting:ChatSettingModel){
        settings = setting
    }
    
    public func setConfigSettings(config:ChatConfigModel){
        configSettings = config
    }
    
    public init(enterpriseId:String,conversationId:String,roles:String) {
        self.enterpriseId = enterpriseId
        self.userRole = roles
        self.conversationId = conversationId
        CONVERSATION_ID = self.conversationId
    }
    
    public func login(email:String,password:String, success: @escaping (_ response: Any) -> Void, failure: @escaping (_ error: String) -> Void, exception: @escaping (_ error: String) -> Void){
        self.userEmail = email
        self.userPassword = password
        let att = ["email","mobile_number"]
        
        let params = ["enterprise_id":enterpriseId, "user_id":userEmail, "password":userPassword, "roles":userRole, "attrs":configSettings.LOGIN_ATTRS ?? ["email","mobile_number","user_id"]] as Dictionary<String, Any>

        var request = URLRequest(url: URL(string: BASE_URL + API.AUTH_API)!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil{
                print(response!)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                        if let error = json["error"] as? String{
                            failure(error)
                        }
                        else{
                            GlobalVars.objLoginModel = ClsLoginAuthModel(fromDictionary: json as NSDictionary)
                            GlobalVars.api_key = GlobalVars.objLoginModel.apiKey
                            GlobalVars.user_id = GlobalVars.objLoginModel.userId
                            GlobalVars.enterprise_id = GlobalVars.objLoginModel.enterpriseId
                            GlobalVars.objLoginModel.email = email
                            setCustomObject(value: GlobalVars.objLoginModel, key: GlobalVars.USER_LOGIN_INFO)
                            success(json)
                        }
                    }
                } catch {
                    exception("json is not serializable")
                    print("error")
                }
            }
            else{
                failure(error!.localizedDescription)
            }
        })

        task.resume()
    }
    
    public func isAlreadyLogin()->Bool{
        return getCustomObject(key: GlobalVars.USER_LOGIN_INFO) == nil ? false : true
    }
}
