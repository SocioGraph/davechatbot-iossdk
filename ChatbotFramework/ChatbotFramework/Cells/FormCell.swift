//
//  CollectionCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 05/10/21.
//

import UIKit

class FormCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "FormCell", bundle: bundle)
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!{
        didSet{
            imgArrow.changeColor(UIColor.init(hexString: "eff5fc"))
        }
    }
    
    @IBOutlet weak var stackView: UIStackView!
    
    var customerState:String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageView.setRadius(radius: 5.0, corners: [.layerMaxXMaxYCorner,
                                                            .layerMaxXMinYCorner,
                                                            .layerMinXMinYCorner])
        // Initialization code
        DispatchQueue.main.async {
            self.imgProfile.cornerRadius = self.imgProfile.frame.size.width/2.0
        }
        
        // Initialization code
        setupUI()
    }

    func setupUI(){
        imgProfile.image = chatScreenSettings.ChatOpponantProfile
        
        messageView.backgroundColor = chatScreenSettings.MessageBgColor
        
        lblMessage.font = chatScreenSettings.MessageFont
        lblMessage.textColor = chatScreenSettings.MessageFontColor
        
        lblSenderName.font = chatScreenSettings.NameAndTimeFont
        lblSenderName.textColor = chatScreenSettings.NameAndTimeFontColor
        
        lblTime.font = chatScreenSettings.NameAndTimeFont
        lblTime.textColor = chatScreenSettings.NameAndTimeFontColor
        
        self.imgProfile.image = chatScreenSettings.AssistantProfile
        self.lblSenderName.text = chatScreenSettings.AssistantName
    }

    func setupCell(_ forms:[ClsFormModel]){
        stackView.isUserInteractionEnabled = true
        stackView.alpha = 1.0
        
        for view in self.stackView.subviews {
            view.removeFromSuperview()
        }
         var index = -1
        for form in forms {
            index += 1
            if form.uiElement == "number" || form.uiElement == "text"{
                let txt = OptionTextfieldClass()
                txt.textField.placeholder = form.placeholder
                txt.textField.text = form.text
                txt.formModel = form
                stackView.addArrangedSubview(txt)
            }
            else if form.uiElement == "textarea"{
                let txt = OptionTextViewClass()
                txt.textView.placeholder = form.placeholder
                txt.textView.text = form.text
                txt.formModel = form
                stackView.addArrangedSubview(txt)
            }
            else if form.uiElement == "datetime" || form.uiElement == "date"{
                let txt = OptionDateTimeClass()
                txt.isOnlyDate = form.uiElement == "date" ? true : false
                txt.textField.placeholder = form.placeholder
                if form.value != ""{
                    txt.textField.text = form.value.getDateFromLocalFormat(format: (form.value.contains(":") ? "yyyy-MM-dd HH:mm" : "yyyy-MM-dd"), toFormat: "dd/MM/yyyy HH:mm")
                }
                
                var minDate:String = ""
                if form.minDate != "" && form.minTime != ""{
                    minDate = form.minDate + " " + form.minTime
                }
                
                var maxDate:String = ""
                if form.maxDate != "" && form.maxTime != ""{
                    maxDate = form.maxDate + " " + form.maxTime
                }
                
                txt.setupView(form.value, minDateStr: minDate, maxDateStr: maxDate)
                txt.formModel = form
                stackView.addArrangedSubview(txt)
            }
            else if form.uiElement == "select"{
                let txt = OptionDropDownClass()
                txt.txtOptions.placeholder = form.placeholder
                txt.txtOptions.text = form.text
                txt.formModel = form
                stackView.addArrangedSubview(txt)
            }
            else if form.uiElement == "file"{
                let txt = OptionChooseFileClass()
                txt.formModel = form
                txt.btnChooseFile.tag = index
                if form.fileUrl != ""{
                    if let url = URL(string: form.fileUrl){
                        txt.lblFileName.text = url.fileName
                    }
                }
                txt.didTapChooseFile = { model, index in
                    
                }
                stackView.addArrangedSubview(txt)
            }
            else if form.uiElement == "geolocation"{
                let txt = OptionLocationClass()
                txt.txtLocation.text = form.text
                txt.formModel = form
                stackView.addArrangedSubview(txt)
            }
        }
        
        let btn = SubmitButtonClass()
        btn.btnSubmit.addTarget(self, action: #selector(onBtnSubmitForm(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(btn)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnSubmitForm(_ sender: UIButton) {
        self.endEditing(true)
        guard let chatVC = self.parentViewController() as? ChatVC else {
            return
        }
        chatVC.startUserInteractionTimer()
        let requestDict = NSMutableDictionary()
        var uploadFileModels:[ClsFormModel] = []
        for txt in stackView.subviews {
            if let txtFeild = txt as? OptionTextfieldClass{
                let value = txtFeild.textField.text!.string
                if txtFeild.formModel.required {
                    if value == ""{
                        chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                        return
                    }
                }
                if txtFeild.formModel.name == "mobile_number" && txtFeild.textField.text!.empty{
                    chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                    return
                }
                if txtFeild.formModel.name == "email" && !value.validEmail{
                    chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                    return
                }
                if let key = txtFeild.formModel.name{
                    requestDict.setValue(value, forKey: key)
                }
            }
            else if let txtFeild = txt as? OptionTextViewClass{
                let value = txtFeild.textView.text!.string
                if txtFeild.formModel.required {
                    if value == ""{
                        chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                        return
                    }
                }
                if let key = txtFeild.formModel.name{
                    requestDict.setValue(value, forKey: key)
                }
            }
            else if let txtFeild = txt as? OptionDateTimeClass{
                var value = ""
                if txtFeild.selectedDate != nil{
                    value = txtFeild.selectedDate.getStrDateFromDate(format: "MMM dd, yyyy HH:mm")
                }
                
                if txtFeild.formModel.required {
                    if value == ""{
                        chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                        return
                    }
                }
                if let key = txtFeild.formModel.name{
                    requestDict.setValue(value, forKey: key)
                }
            }
            else if let txtFeild = txt as? OptionDropDownClass{
                if txtFeild.formModel.required {
                    if txtFeild.txtOptions.text! == ""{
                        chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                        return
                    }
                }
                if let value = txtFeild.optionsArr.first(where: {$0.value == txtFeild.txtOptions.text!}){
                    if let key = txtFeild.formModel.name{
                        requestDict.setValue(value.value, forKey: key)
                    }
                }
            }
            else if let txtFeild = txt as? OptionChooseFileClass{
                if txtFeild.formModel.required {
                    if txtFeild.formModel.fileUrl == ""{
                        chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                        return
                    }
                }
                
                uploadFileModels.append(txtFeild.formModel)
            }
            else if let txtFeild = txt as? OptionLocationClass{
                if txtFeild.formModel.required {
                    if txtFeild.formModel.text == ""{
                        chatVC.alertOkay(title: "Alert", message: txtFeild.formModel.error, okAction: nil)
                        return
                    }
                }
                if let value =  txtFeild.formModel.value, value != ""{
                    requestDict.setValue(txtFeild.formModel.value.components(separatedBy: ",")[0], forKey: "lat")
                    requestDict.setValue(txtFeild.formModel.value.components(separatedBy: ",")[1], forKey: "lng")
                }
            }
        }
        
        if uploadFileModels.count > 0 {
            startUploadDocument(urls: uploadFileModels)
        }
        else if let jsonString = (requestDict as Dictionary).jsonStringRepresentation{
            messagesArr[self.tag].data.isEnable = false
            chatVC.sendMessage(msgStr: jsonString, customer_state: messagesArr[self.tag].data.customerState,requestModel: messagesArr[self.tag])
        }
    }
    
    func sendFilesUrl(models:[ClsFormModel]){
        if let vc = self.parentVC as? ChatVC{
            let dict = NSMutableDictionary()
            var message:String = ""
            
            for model in models {
                dict.setValue(model.value, forKey: model.name)
                
                if message != "" {
                    message.append(contentsOf: "\n")
                }
                if let url = URL(string: model.fileUrl){
                    message.append(contentsOf: "file name : \(url.fileName)")
                }
            }
            
            if let strJson = self.json(from: dict){
                messagesArr[self.tag].data.isEnable = false
                vc.removeUserPlaceholder(isReload: false)
                vc.sendMessage(msgStr: strJson, customer_state: self.customerState,displayMsg: message)
            }
            
            
        }
    }
    
    func startUploadDocument(urls:[ClsFormModel]){
        
        if let vc = self.parentVC as? ChatVC{
            vc.addUserPlaceholder()
        }
        
        
        var numberOfUploadedFiles = 0
        for model in urls {
            uploadingDocument(model: model) { data,model1 in
                if let dict = data as? NSDictionary{
                    if let modell = urls.first(where: {$0 == model1}){
                        if let path = dict.value(forKey: "path") as? String{
                            numberOfUploadedFiles += 1
                            modell.value = path
                            if numberOfUploadedFiles == urls.count{
                                DispatchQueue.main.async{
                                    self.sendFilesUrl(models: urls)
                                }
                            }
                        }
                    }
                }
            } failure: { error in
                
            }
        }
    }
    
    func uploadingDocument(model:ClsFormModel,success: @escaping (_ response: Any, _ model:ClsFormModel) -> Void, failure: @escaping (_ error: String) -> Void){
        let request = MultipartFormDataRequest(url: URL(string: BASE_URL + API.UPLOAD_DOCUMENT_API)!)
        
        guard let url = URL(string: model.fileUrl) else { return }
        
        guard let data = url.loadFileFromLocalPath() else { return }
        
        request.addDataField(named: "file",fileName: "file.\(url.pathExtension)", data: data, mimeType: (url as NSURL).mimeType())
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                            success(json,model)
                        }
                    } catch {
                        failure("something want to wrong")
                    }
                }
                else{
                    failure("something want to wrong")
                }
            }
            else{
                failure(error.debugDescription)
            }
        })
        task.resume()
    }
    
    func json(from object: Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

