//
//  VideoMsgCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 29/09/21.
//

import UIKit
import MediaPlayer
import AVKit
class VideoMsgCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "VideoMsgCell", bundle: bundle)
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!{
        didSet{
            imgArrow.changeColor(UIColor.init(hexString: "eff5fc"))
        }
    }
    @IBOutlet weak var videoPlayerView: UIView!
    
    
    @IBOutlet weak var imgVideoThumbnail: UIImageView!
    
    var playerLayer = AVPlayerLayer()
    var videoUrl:String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageView.setRadius(radius: 5.0, corners: [.layerMaxXMaxYCorner,
                                                            .layerMaxXMinYCorner,
                                                            .layerMinXMinYCorner])
        // Initialization code
        DispatchQueue.main.async {
            self.imgProfile.cornerRadius = self.imgProfile.frame.size.width/2.0
        }
        setupUI()
    }

    func setupUI(){
        imgProfile.image = chatScreenSettings.ChatOpponantProfile
        
        messageView.backgroundColor = chatScreenSettings.MessageBgColor
        
        lblMessage.font = chatScreenSettings.MessageFont
        lblMessage.textColor = chatScreenSettings.MessageFontColor
        
        lblSenderName.font = chatScreenSettings.NameAndTimeFont
        lblSenderName.textColor = chatScreenSettings.NameAndTimeFontColor
        
        lblTime.font = chatScreenSettings.NameAndTimeFont
        lblTime.textColor = chatScreenSettings.NameAndTimeFontColor
        
        self.lblSenderName.text = chatScreenSettings.AssistantName
        self.imgProfile.image = chatScreenSettings.AssistantProfile
    }
    
    func setupView(_ url:String){
        videoUrl = url
        if let image = self.createVideoThumbnail(from: URL(string: url)!){
            imgVideoThumbnail.image = image
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnPlayVideo(_ sender: UIButton) {
        if let vc = self.parentVC as? ChatVC{
            vc.startUserInteractionTimer()
        }
        let controller = AVPlayerViewController()
        controller.player = AVPlayer(url: URL(string: videoUrl)!)
        controller.player?.play()
        self.parentViewController()?.present(controller, animated: true)
    }
    
    func createVideoThumbnail(from url: URL) -> UIImage?{
        
        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.maximumSize = CGSize(width: frame.width, height: frame.height)
        
        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
        
    }
}



