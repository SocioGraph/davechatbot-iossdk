//
//  ImageCollectionCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 05/10/21.
//

import UIKit
import SafariServices
class ImageCollectionCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "ImageCollectionCell", bundle: bundle)
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!{
        didSet{
            imgArrow.changeColor(UIColor.init(hexString: "eff5fc"))
        }
    }
    
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageView.setRadius(radius: 5.0, corners: [.layerMaxXMaxYCorner,
                                                            .layerMaxXMinYCorner,
                                                            .layerMinXMinYCorner])
        // Initialization code
        DispatchQueue.main.async {
            self.imgProfile.cornerRadius = self.imgProfile.frame.size.width/2.0
        }
        
        // Initialization code
        setupUI()
    }

    func setupUI(){
        imgProfile.image = chatScreenSettings.ChatOpponantProfile
        
        messageView.backgroundColor = chatScreenSettings.MessageBgColor
        
        lblMessage.font = chatScreenSettings.MessageFont
        lblMessage.textColor = chatScreenSettings.MessageFontColor
        
        lblSenderName.font = chatScreenSettings.NameAndTimeFont
        lblSenderName.textColor = chatScreenSettings.NameAndTimeFontColor
        
        lblTime.font = chatScreenSettings.NameAndTimeFont
        lblTime.textColor = chatScreenSettings.NameAndTimeFontColor
        
        self.imgProfile.image = chatScreenSettings.AssistantProfile
        self.lblSenderName.text = chatScreenSettings.AssistantName
    }

    var imageArr:[ClsThumbnail]?
    func setupCell(imageArr:[ClsThumbnail]){
        
        for view in self.stackView.subviews {
            view.removeFromSuperview()
        }
        
        var j = 0
        if imageArr.count > 1 {
            for _ in 0..<Int(round(CGFloat(imageArr.count)/2.0)) {
                let img = OptionImageClass()
                img.imgView2.isHidden = true
                if imageArr.count - 1 >= j {
                    img.img1.downloaded(from: URL(string: imageArr[j].image)!)
                    img.lblImgName1.text = imageArr[j].title
                    img.img1.accessibilityValue = imageArr[j].image
                    j += 1
                }
                
                if imageArr.count - 1 >= j {
                    img.imgView2.isHidden = false
                    img.img2.downloaded(from: URL(string: imageArr[j].image)!)
                    img.lblImgName2.text = imageArr[j].title
                    img.img2.accessibilityValue = imageArr[j].image
                    j += 1
                }
                
                img.didSelectImage = { urlStr in
                    if let vc = self.parentViewController() as? ChatVC{
                        vc.startUserInteractionTimer()
                        let sf = SFSafariViewController(url: URL(string: urlStr)!)
                        sf.delegate = self
                        vc.present(sf, animated: true, completion: nil)
                    }
                }
                
                self.stackView.addArrangedSubview(img)
            }
        }
        else{
            let img = OptionImageClass()
            img.imgView2.isHidden = true
            img.img1.downloaded(from: URL(string: imageArr[0].image)!)
            img.img1.accessibilityValue = imageArr[0].image
            img.didSelectImage = { urlStr in
                if let vc = self.parentViewController() as? ChatVC{
                    vc.startUserInteractionTimer()
                    let sf = SFSafariViewController(url: URL(string: urlStr)!)
                    sf.delegate = self
                    vc.present(sf, animated: true, completion: nil)
                }
            }
            self.stackView.addArrangedSubview(img)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ImageCollectionCell:SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
