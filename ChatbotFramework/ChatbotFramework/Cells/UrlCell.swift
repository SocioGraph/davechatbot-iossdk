//
//  TypingLoaderSenderCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 10/09/21.
//

import UIKit
import SafariServices
class UrlCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "UrlCell", bundle: bundle)
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!{
        didSet{
            imgArrow.changeColor(UIColor.init(hexString: "eff5fc"))
        }
    }
    
    @IBOutlet weak var lblUrl: UILabel!
    
    
    var urlStr = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageView.setRadius(radius: 5.0, corners: [.layerMaxXMaxYCorner,
                                                            .layerMaxXMinYCorner,
                                                            .layerMinXMinYCorner])
        // Initialization code
        DispatchQueue.main.async {
            self.imgProfile.cornerRadius = self.imgProfile.frame.size.width/2.0
        }
        setupUI()
    }

    func setupUI(){
        imgProfile.image = chatScreenSettings.ChatOpponantProfile
        
        messageView.backgroundColor = chatScreenSettings.MessageBgColor
        
        lblMessage.font = chatScreenSettings.MessageFont
        lblMessage.textColor = chatScreenSettings.MessageFontColor
        
        lblSenderName.font = chatScreenSettings.NameAndTimeFont
        lblSenderName.textColor = chatScreenSettings.NameAndTimeFontColor
        
        lblTime.font = chatScreenSettings.NameAndTimeFont
        lblTime.textColor = chatScreenSettings.NameAndTimeFontColor
        
        self.imgProfile.image = chatScreenSettings.AssistantProfile
        self.lblSenderName.text = chatScreenSettings.AssistantName
    }

    func setupCell(_ urlStr:String){
        self.urlStr = urlStr
        
        let astrMessage = NSMutableAttributedString(string: urlStr)

        //set some attributes
        astrMessage.addAttribute(NSAttributedString.Key.foregroundColor,
                                 value: UIColor.blue,
                                 range: NSRange(location: 0, length: (urlStr.count)))
        astrMessage.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: (urlStr.count)))
        lblUrl.attributedText = astrMessage
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBtnClickUrl(_ sender: UIButton) {
        if let vc = self.parentViewController() as? ChatVC{
            vc.startUserInteractionTimer()
            let sf = SFSafariViewController(url: URL(string: urlStr)!)
            sf.delegate = self
            vc.present(sf, animated: true, completion: nil)
        }
    }
}

extension UrlCell:SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
