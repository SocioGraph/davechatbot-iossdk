//
//  CollectionCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 05/10/21.
//

import UIKit

class CollectionCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "CollectionCell", bundle: bundle)
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!{
        didSet{
            imgArrow.changeColor(UIColor.init(hexString: "eff5fc"))
        }
    }
    @IBOutlet weak var stackView: UIStackView!
    
    var didSelect:(OptionButtonClass,Int) -> Void = {_,_ in}
    var options:[[String]]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageView.setRadius(radius: 5.0, corners: [.layerMaxXMaxYCorner,
                                                            .layerMaxXMinYCorner,
                                                            .layerMinXMinYCorner])
        // Initialization code
        DispatchQueue.main.async {
            self.imgProfile.cornerRadius = self.imgProfile.frame.size.width/2.0
        }
        
        // Initialization code
        setupUI()
    }

    func setupUI(){
        imgProfile.image = chatScreenSettings.ChatOpponantProfile
        
        messageView.backgroundColor = chatScreenSettings.MessageBgColor
        
        lblMessage.font = chatScreenSettings.MessageFont
        lblMessage.textColor = chatScreenSettings.MessageFontColor
        
        lblSenderName.font = chatScreenSettings.NameAndTimeFont
        lblSenderName.textColor = chatScreenSettings.NameAndTimeFontColor
        
        lblTime.font = chatScreenSettings.NameAndTimeFont
        lblTime.textColor = chatScreenSettings.NameAndTimeFontColor
        
        self.imgProfile.image = chatScreenSettings.AssistantProfile
        self.lblSenderName.text = chatScreenSettings.AssistantName
    }
    
    func setupCell(_ options:[[String]]){
        for view in self.stackView.subviews {
            view.removeFromSuperview()
        }
        
        
        self.options = options
        for optionArr in options {
            if optionArr.count > 1 {
                let btn = OptionButtonClass()
                btn.lblOption.text = optionArr[1]
                btn.accessibilityLabel = optionArr[1]
                btn.accessibilityValue = optionArr[0]
                btn.lblOption.textAlignment = .center
                btn.didSelect = { selectBtn in
                    self.didSelect(selectBtn,self.tag)
//                    if let vc = self.parentViewController() as? ChatVC{
//                        if let value = selectBtn.accessibilityValue, let label = selectBtn.accessibilityLabel{
//                            vc.sendMessage(msgStr: label, customer_state: value)
//                        }
//                    }
                }
                stackView.addArrangedSubview(btn)
            }
        }
    }
    
    @IBAction func onClick(_ sender: UIButton) {
        print(sender.titleLabel?.text ?? "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
