//
//  TypingLoaderSenderCell.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 10/09/21.
//

import UIKit

class ChatSenderCell: UITableViewCell {
    //MARK:- NIB
    static var nib : UINib {
        get {
            let frameworkBundleID  = "chatbotFramework";
            let bundle = Bundle(identifier: frameworkBundleID)
            return UINib(nibName: "ChatSenderCell", bundle: bundle)
        }
    }
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!{
        didSet{
            imgArrow.changeColor(UIColor.init(hexString: "eff5fc"))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.messageView.setRadius(radius: 5.0, corners: [.layerMinXMaxYCorner,
                                                            .layerMaxXMinYCorner,
                                                            .layerMinXMinYCorner])
        // Initialization code
        DispatchQueue.main.async {
            self.imgProfile.cornerRadius = self.imgProfile.frame.size.width/2.0
        }
        
        setupUI()
    }

    func setupUI(){
        
        imgProfile.image = chatScreenSettings.ChatUserProfile
        
        
        messageView.backgroundColor = chatScreenSettings.MessageBgColor
        
        lblMessage.font = chatScreenSettings.MessageFont
        lblMessage.textColor = chatScreenSettings.MessageFontColor
        
        lblSenderName.font = chatScreenSettings.NameAndTimeFont
        lblSenderName.textColor = chatScreenSettings.NameAndTimeFontColor
        
        lblTime.font = chatScreenSettings.NameAndTimeFont
        lblTime.textColor = chatScreenSettings.NameAndTimeFontColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
