//
//  ChatButton.swift
//  chatbotFramework
//
//  Created by Manish's Mac on 01/09/21.
//

import UIKit
public class ChatButton: UIView {
    //MARK:- Outlet
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var view: UIView!
    
    //MARK:- Veriable Declaration
    public var UpdateSize: CGSize = CGSize(width: 80.0, height: 80.0){
        didSet {
            updateView()
        }
    }
    
    public var setDistance: CGFloat = 10{
        didSet {
            updateView()
        }
    }
    
    public var position: enumPositions = .rightBottom{
        didSet {
            updateView()
        }
    }
    
    public var customPosition: CGPoint = CGPoint(x: 50, y: 50){
        didSet {
            updateView()
        }
    }
    
    
    public var addImage: UIImage?{
        didSet {
            updateView()
        }
    }
    
    public var setBackgroundColor: UIColor = .clear{
        didSet {
            self.btn.backgroundColor = setBackgroundColor
        }
    }
    
    public var quickChatTitle:String = "this is option"
    
    public var userEmail:String = ""
    public var userPassword:String = ""
    public var userRole:String = ""
    public var enterpriseId:String = ""
    
    
    //MARK:- UIView life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        bundle!.loadNibNamed("ChatButton", owner: self, options: nil)
        self.addSubview(view)
        btn.tintColor = .white
        self.addConstraints()
        updateView()
        
        if getCustomObject(key: GlobalVars.USER_LOGIN_INFO) != nil{
            GlobalVars.objLoginModel = getCustomObject(key: GlobalVars.USER_LOGIN_INFO) as! ClsLoginAuthModel
            GlobalVars.api_key = GlobalVars.objLoginModel.apiKey
            GlobalVars.user_id = GlobalVars.objLoginModel.userId
            GlobalVars.enterprise_id = GlobalVars.objLoginModel.enterpriseId
        }
        
        if getCustomObject(key: "messages_arr") == nil {
            self.callHistoryAPI()
        }
        if getCustomObject(key: "conversation_keyword_arr") == nil{
            getKeywordsAPI()
        }
        else{
            setConversationKeywords()
        }
    }
    
    func updateView() {
        self.frame.size = UpdateSize
        self.view.layoutIfNeeded()
        if let image = self.addImage{
            self.btn.setImage(image, for: .normal)
        }
        
        var safeAreaBottom:CGFloat = 0.0
        var safeAreaTop:CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            if UIApplication.shared.windows.count > 0{
                let window = UIApplication.shared.windows[0]
                safeAreaBottom = window.safeAreaInsets.bottom
                safeAreaTop = window.safeAreaInsets.top
            }
        }
        
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        switch position {
        case .leftTop:
            self.frame.origin = CGPoint(x: setDistance, y: safeAreaTop + setDistance)
            break
        case .rightTop:
            self.frame.origin = CGPoint(x: screenSize.size.width - self.bounds.size.width - setDistance, y: safeAreaTop + setDistance)
            break
        case .leftCenter:
            self.frame.origin = CGPoint(x: setDistance, y: screenSize.height / 2.0 - self.bounds.size.height / 2.0)
            break
        case .rightCenter:
            self.frame.origin = CGPoint(x: screenSize.size.width - self.bounds.size.width - setDistance, y: screenSize.height / 2.0 - self.bounds.size.height / 2.0)
            break
        case .leftBottom:
            self.frame.origin = CGPoint(x: 10, y: screenSize.size.height - self.bounds.size.height - safeAreaBottom - setDistance)
            break
        case .rightBottom:
            self.frame.origin = CGPoint(x: screenSize.size.width - self.bounds.size.width - setDistance, y: screenSize.size.height - self.bounds.size.height - safeAreaBottom - setDistance)
            break
        case .custom:
            self.frame.origin = self.customPosition
            break
        }
        
        self.layer.cornerRadius = self.frame.size.width / 2.0
        self.layer.masksToBounds = true
    }
    
    func addConstraints(){
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    
    @IBAction func onBtnQuickChat(_ sender: UIButton) {
        let vc = UIApplication.shared.windows.first!.rootViewController!
        let chatVC = ChatVC.UI
        chatVC.quickChatTitle = self.quickChatTitle
        vc.present(ChatVC.UI, animated: true)
        
    }
    
    open func logout(){
        GlobalVars.engagementId = ""
        GlobalVars.systemResponse = ""
        removeObjectForKey("messages_arr")
        removeObjectForKey("quick_chat")
        removeObjectForKey("_page_number")
        removeObjectForKey("isFinishedRecord")
        removeObjectForKey(GlobalVars.USER_LOGIN_INFO)
    }
}

//MARK:- API
extension ChatButton{

    func callHistoryAPI(){
        if let vc = self.parentViewController(){
            vc.present(LoaderVC.UI, animated: false)
        }
        getConversationHistory { messagesArr in
            DispatchQueue.main.async {
                if let vc = self.parentViewController(){
                    vc.dismiss(animated: false)
                }
            }
            var dict:[NSDictionary] = []
            for model in messagesArr {
                dict.append(model.toDictionary())
            }
            setCustomObject(value: dict as AnyObject, key: "messages_arr")
        }
    }
    
    func getKeywordsAPI(){
        var request = URLRequest(url: URL(string: BASE_URL + API.CONVERSATION_KEYWORD_API)!)
        request.httpMethod = "GET"
        
        for (key,value) in ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                            "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                            "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
                           ] {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

//        let params = ["limit":"50","offset":"0"]
//
//        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            var modelArr:[ClsConversationModel] = []
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                            
                            setCustomObject(value: json as AnyObject, key: "conversation_keyword_arr")
                            
                            self.setConversationKeywords()
                        }
                    } catch {
                        print("error")
                    }
                }
                else{
                    
                }
            }
            else{
                print(error.debugDescription)
            }
//            completion(modelArr)
        })

        task.resume()
    }
    
    public func submitFeedback(rate:Int,feedback:String,completion: @escaping (_ response: Bool,_ error:String) -> Void){
        
        if rate == 0{
            completion(false,"Please Do Give Star Ratings!")
            return
        }
        
        var request = URLRequest(url: URL(string: BASE_URL + API.FEEDBACK_API + "/" + GlobalVars.engagementId)!)
        request.httpMethod = "PATCH"
        
        for (key,value) in ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                            "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                            "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
                           ] {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let params = ["accuracy_rating":rate,"usefulness_rating":rate,"feedback":feedback] as [String : Any]

        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil{
                print(response!)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                        if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                            completion(true,"")
                        }
                        else{
                            completion(false,"")
                        }
                    }
                } catch {
                    completion(false,"JSON could not be serialized")
                }
                
            }
            else{
                print(error.debugDescription)
                completion(false,error?.localizedDescription ?? "")
            }
        })

        task.resume()
    }
    
    func getConversationHistory(completion: @escaping (_ response: [ClsConversationModel]) -> Void){
        
        let pageNumber = getCustomObject(key: "_page_number") as? Int ?? 1
        
        var request = URLRequest(url: URL(string: BASE_URL + API.CONVERSATION_HISTORY_API + "/" + GlobalVars.objLoginModel.userId + "?_page_size=20&_page_number=\(pageNumber)")!)
        request.httpMethod = "GET"
        
        for (key,value) in ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                            "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                            "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
                           ] {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let params = ["_page_size":20,"_page_number":1] as Dictionary<String, Any>
//
//        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            var modelArr:[ClsConversationModel] = []
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                            if let historyArr = json["history"] as? [NSDictionary], historyArr.count > 0{
                                setCustomObject(value: pageNumber as AnyObject, key: "_page_number")

                                for dict in historyArr {
                                    let msg = ClsConversationModel(fromDictionary: dict)
                                    msg.data.isEnable = false//for form
                                    modelArr.append(msg)
                                }
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                else{
                    
                }
            }
            else{
                print(error.debugDescription)
            }
            completion(modelArr)
        })

        task.resume()
    }
    
    func setConversationKeywords(){
        if let json = getCustomObject(key: "conversation_keyword_arr") as? Dictionary<String, AnyObject>{
            if let keywordArr = json["keywords"] as? NSArray, keywordArr.count > 0{
                GlobalVars.conversationKeywordArr = []
                
                for dict in keywordArr {
                    if let keywordArrs = dict as? NSArray{
                        let model = ClsKeyValue(key: keywordArrs[2] as! String, value: keywordArrs[0] as! String,type: keywordArrs[1] as! String)
                        GlobalVars.conversationKeywordArr.append(model)
                    }
                }
            }
        }
    }
    
    
    func getStringFromArr(arr:[String]) -> String{
        var myJsonString = ""
            do {
                let data =  try JSONSerialization.data(withJSONObject:arr, options: .prettyPrinted)
                myJsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
            } catch {
                print(error.localizedDescription)
            }
            return myJsonString
    }
    
    
    
    public func sendMessage(msgStr:String,customer_state:String = ""){
        let msg = ClsConversationModel(fromDictionary: NSDictionary())
        msg.customerResponse = msgStr
        msg.isSend = true
        msg.direction = "user"
        msg.timestamp = Date().getCurrentGMTTime()
        
        if msg.customerResponse != ""{
//            messagesArr.append(msg)
            if var previous_arr = getCustomObject(key: "messages_arr") as? [NSDictionary]
            {
                previous_arr.insert(msg.toDictionary(), at: previous_arr.count)
                setCustomObject(value: previous_arr as AnyObject, key: "messages_arr")
            }
        }
        
        let header:NSDictionary = ["x-i2ce-api-key":GlobalVars.objLoginModel.apiKey as AnyObject,
                                   "x-i2ce-enterprise-id":GlobalVars.objLoginModel.enterpriseId as AnyObject,
                                   "x-i2ce-user-id":GlobalVars.objLoginModel.userId as AnyObject
        ]
        
        
        
        getConversationText(text: msgStr,customerState: customer_state, header: header)
    }
}


public enum enumPositions {
    
    case leftTop
    case rightTop
    case leftCenter
    case rightCenter
    case leftBottom
    case rightBottom
    case custom
}

//MARK:- API
extension ChatButton{
    func getConversationText(text:String = "a",customerState:String = "",header:NSDictionary = [:]){
        
        var request = URLRequest(url: URL(string: BASE_URL + API.CONVERSATION_API + "/" + "aslkdfjalskjdf")!)
        request.httpMethod = "POST"
        for (key,value) in header {
            request.setValue("\(value)", forHTTPHeaderField: "\(key)")
        }
        if GlobalVars.engagementId != "" {
            let params = ["system_response":GlobalVars.systemResponse,"engagement_id":GlobalVars.engagementId,"customer_state":customerState,"customer_response":text]
            
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil{
                print(response!)
                if let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!) as? Dictionary<String, AnyObject>{
                            if var previous_arr = getCustomObject(key: "messages_arr") as? [NSDictionary]
                            {
                                let conversationModel = ClsConversationModel(fromDictionary: json as NSDictionary)
                                
                                GlobalVars.systemResponse = conversationModel.name
                                GlobalVars.engagementId = conversationModel.engagementId
                                
                                previous_arr.insert(conversationModel.toDictionary(), at: previous_arr.count)
                                setCustomObject(value: previous_arr as AnyObject, key: "messages_arr")
                                
                                DispatchQueue.main.async {
                                    self.onBtnQuickChat(UIButton())
                                }
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                else{
                    
                }
            }
            else{
                print(error.debugDescription)
            }
        })

        task.resume()
    }
}


public class ChatSettingModel{
    
    /**
     Chat screen background color
     */
    public var ChatViewBgColor:UIColor!
    
    /**
     Update assistant name
     */
    public var AssistantName:String!
    
    /**
     Update assistant profile
     */
    public var AssistantProfile:UIImage!
    
    /**
     Update top header bar color
     */
    public var HeaderBarColor:UIColor!
    
    /**
     Update close button icon
     */
    public var CloseChatBgImage:UIImage!
    
    /**
     Opponent bot profile image
     */
    public var ChatOpponantProfile:UIImage!
    
    /**
     Current user profile image
     */
    public var ChatUserProfile:UIImage!
    
    /**
     Update user name and time font
     */
    public var NameAndTimeFont:UIFont!
    
    /**
     Update user name and time font color
     */
    public var NameAndTimeFontColor:UIColor!
    
    /**
     Update chat message font
     */
    public var MessageFont:UIFont!
    
    /**
     Update chat message font color
     */
    public var MessageFontColor:UIColor!
    
    /**
     Update chat message background color
     */
    public var MessageBgColor:UIColor!
    
    /**
     Change quick chat title text  "this is option"
     */
    public var QuickChatTitle:String!
    
    /**
     Change quick chat title font
     */
    public var QuickChatFont:UIFont!
    
    /**
     Change quick chat  text color
     */
    public var QuickChatTextColor:UIColor!
    
    /**
     Change quick chat option background color
     */
    public var QuickChatListBgColor:UIColor!
    
    /**
    Change options text color
     */
    public var OptionsListTextColor:UIColor!
    
    /**
     Change typing textfield placeholder text
     */
    public var TypeTextfieldPlaceholderText:String!
    
    /**
     Change typing textfieldtext color
     */
    public var TypeTextfieldTextColor:UIColor!
    
    /**
     Change typing textfieldtext font
     */
    public var TypeTextfieldTextFont:UIFont!
    
    /**
     Change send button bg image.
     */
    public var SendButtonBgImage:UIImage!
    
    /**
     Change feedback star color
     */
    public var FeedbackStarColor:UIColor!
    
    public init() {
        let frameworkBundleID  = "chatbotFramework";
        let bundle = Bundle(identifier: frameworkBundleID)
        
        ChatViewBgColor = .white
        AssistantName = "Aldo Virtual Assistant"
        HeaderBarColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        AssistantProfile = UIImage(named: "dave-icon", in: bundle, compatibleWith: nil)!
        CloseChatBgImage = UIImage(named: "close", in: bundle, compatibleWith: nil)!
        ChatOpponantProfile = UIImage(named: "dave-icon", in: bundle, compatibleWith: nil)!
        ChatUserProfile = UIImage(named: "avtar", in: bundle, compatibleWith: nil)!
        NameAndTimeFont = UIFont.systemFont(ofSize: 8.0)
        NameAndTimeFontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        MessageFont = UIFont.systemFont(ofSize: 12.0)
        MessageFontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        MessageBgColor = #colorLiteral(red: 0.5764705882, green: 0.7450980392, blue: 0.8666666667, alpha: 1)
        QuickChatTitle = "this is option"
        QuickChatFont = UIFont.systemFont(ofSize: 12.0)
        QuickChatTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        QuickChatListBgColor = #colorLiteral(red: 0.7215686275, green: 0.9294117647, blue: 1, alpha: 1)
        OptionsListTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        TypeTextfieldPlaceholderText = "Type here"
        TypeTextfieldTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        TypeTextfieldTextFont = UIFont.systemFont(ofSize: 17.0)
        SendButtonBgImage = UIImage(named: "send", in: bundle, compatibleWith: nil)!
        FeedbackStarColor = #colorLiteral(red: 0.5764705882, green: 0.7450980392, blue: 0.8666666667, alpha: 1)
    }
}


public class ChatConfigModel{
    public var BASE_API_URL: String!
    public var SIGNUP_API_KEY: String!
    public var ENTERPRISE_ID: String!
    public var CONVERSATION_ID: String!
    public var SPEECH_SERVER: String!
    public var USER_MODEL_ROLE: String!
    public var USER_ID_ATTR: String!
    public var USER_EMAIL_ATTR: String!
    public var USER_PHONE_NUMBER_ATTR: String!
    public var SESSION_MODEL: String!
    public var SESSION_ID: String!
    public var SESSION_USER_ID: String!
    public var INTERACTION_MODEL: String!
    public var INTERACTION_USER_ID: String!
    public var INTERACTION_SESSION_ID: String!
    public var INTERACTION_STAGE_ATTR: String!
    public var DEFAULT_INTERACTION_DATA: String!
    public var LANGUAGE: String!
    public var VOICE_ID: String!
    public var MAX_SPEECH_DURATION: Int64!
    public var SESSION_ORIGIN: String!
    public var INTERACTION_ORIGIN: String!
    public var AVATAR_ID: String!
    public var AVATAR_MODEL: String!
    public var GLB_ATTR: String!
    public var EVENT_MODEL: String!
    public var EVENT_SESSION_ID: String!
    public var EVENT_USER_ID: String!
    public var EVENT_DESCRIPTION: String!
    public var EVENT_WAIT_TIME: String!
    public var EVENT_ORIGIN: String!
    public var DEFAULT_EVENT_DATA: String!
    public var EVENT_TIMER: String!
    public var DEFAULT_USER_DATA: [ClsKeyValue]!
    public var DEFAULT_SESSION_DATA: DefaultSessionData!
    public var LOGIN_ATTRS:[String]!
    
    public init() {
        BASE_API_URL = ""
        SIGNUP_API_KEY = ""
        ENTERPRISE_ID = ""
        CONVERSATION_ID = ""
        SPEECH_SERVER = nil
        USER_MODEL_ROLE = "person"
        USER_ID_ATTR = "user_id"
        USER_EMAIL_ATTR = "email"
        USER_PHONE_NUMBER_ATTR = "mobile_number"
        SESSION_MODEL = "person_session"
        SESSION_ID = "session_id"
        SESSION_USER_ID = "user_id"
        INTERACTION_MODEL = "interaction"
        INTERACTION_USER_ID = "person_id"
        INTERACTION_SESSION_ID = "session_id"
        INTERACTION_STAGE_ATTR = "stage"
        DEFAULT_INTERACTION_DATA = ""
        LANGUAGE = nil
        VOICE_ID = nil
        MAX_SPEECH_DURATION = 10000
        SESSION_ORIGIN = nil
        INTERACTION_ORIGIN = nil
        AVATAR_ID = nil
        AVATAR_MODEL = "avatar"
        GLB_ATTR = "glb_url"
        EVENT_MODEL = nil
        EVENT_SESSION_ID = "session_id"
        EVENT_USER_ID = "user_id"
        EVENT_DESCRIPTION = "event"
        EVENT_WAIT_TIME = "wait_time"
        EVENT_ORIGIN = "origin"
        DEFAULT_EVENT_DATA = nil
        EVENT_TIMER = nil
        DEFAULT_USER_DATA = [ClsKeyValue(key: "person_type", value: "visitor")]
        DEFAULT_SESSION_DATA = DefaultSessionData()
        LOGIN_ATTRS = ["email","mobile_number","user_id"]
    }
}


open class DefaultSessionData{
    var LOCATION_DICT:NSDictionary!
    var BROWSER:String!
    var OS:String!
    var DEVICE_TYPE:String!
    
    init(){
        LOCATION_DICT = NSDictionary()
        BROWSER = ""
        OS = ""
        DEVICE_TYPE = ""
    }
}
